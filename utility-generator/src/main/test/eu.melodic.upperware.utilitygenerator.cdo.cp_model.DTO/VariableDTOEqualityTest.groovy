package eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO

import eu.paasage.upperware.metamodel.cp.VariableType
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO
import spock.lang.Specification


class VariableDTOEqualityTest extends Specification {

    Collection<VariableDTO> variableDTOS = new HashSet<>()
    Collection<GroupingVariableDTO> groupingVariableDTOS = new HashSet<>()

    def setup() {
        List<Integer> domain1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> domain2= new ArrayList<>(Arrays.asList(1,6,9));
        List<Integer> domain3= new ArrayList<>(Arrays.asList(1,6,9));
        VariableDTO variableCardinality1 = new VariableDTO("Component_app", "app", VariableType.CARDINALITY)
        VariableDTO variableCardinality2 = new VariableDTO("Component_db", "db", VariableType.CARDINALITY)
        VariableDTO variableGroupId1 = new GroupingVariableDTO("Component_app", "app", VariableType.GROUP_ID, domain1)
        VariableDTO variableGroupId2 = new GroupingVariableDTO("Component_db", "db", VariableType.GROUP_ID, domain2)
        VariableDTO variableGroupId3 = new GroupingVariableDTO("Component_db", "db", VariableType.GROUP_ID, domain3)
        VariableDTO variableGroupedWith1 = new GroupingVariableDTO("Component_app", "app", VariableType.GROUPED_WITH, domain1)
        VariableDTO variableGroupedWith2 = new GroupingVariableDTO("Component_db", "db", VariableType.GROUPED_WITH, domain2)
        VariableDTO variableGroupedWith3 = new GroupingVariableDTO("Component_db", "db", VariableType.GROUPED_WITH, domain2)


        variableDTOS.add(variableCardinality1)
        variableDTOS.add(variableCardinality2)
        variableDTOS.add(variableGroupId1)
        variableDTOS.add(variableGroupId1)
        variableDTOS.add(variableGroupId2)
        variableDTOS.add(variableGroupId3)
        variableDTOS.add(variableGroupedWith1)
        variableDTOS.add(variableGroupedWith2)
        variableDTOS.add(variableGroupedWith3)

        groupingVariableDTOS.add(variableGroupId1)
        groupingVariableDTOS.add(variableGroupId2)
        groupingVariableDTOS.add(variableGroupId3)

        groupingVariableDTOS.add(variableGroupedWith1)
        groupingVariableDTOS.add(variableGroupedWith2)
        groupingVariableDTOS.add(variableGroupedWith3)
    }

    def "deleting copies"() {

        expect:

        variableDTOS.size() == 6
        groupingVariableDTOS.size() == 4
    }

}
