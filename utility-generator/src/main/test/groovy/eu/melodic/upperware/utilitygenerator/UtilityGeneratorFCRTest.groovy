package groovy.eu.melodic.upperware.utilitygenerator

import eu.melodic.cache.NodeCandidates
import eu.melodic.upperware.penaltycalculator.PenaltyFunctionProperties
import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication
import eu.melodic.upperware.utilitygenerator.properties.UtilityGeneratorProperties
import eu.paasage.upperware.security.authapi.properties.MelodicSecurityProperties
import eu.paasage.upperware.security.authapi.token.JWTService
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.IntVariableValueDTO
import io.github.cloudiator.rest.model.Hardware
import io.github.cloudiator.rest.model.NodeCandidate
import spock.lang.Specification

class UtilityGeneratorFCRTest extends Specification{


    NodeCandidates mockNodeCandidates = GroovyMock(NodeCandidates)
    UtilityGeneratorProperties utilityGeneratorProperties = new UtilityGeneratorProperties()
    MelodicSecurityProperties securityProperties = new MelodicSecurityProperties()
    PenaltyFunctionProperties properties
    JWTService jwtService

    String cardinalityName = "AppCardinality"
    String providerName = "provider_Component_App"

    String dbProviderName = "provider_Component_DB"
    String dbCardinalityName = "cardinality_Component_DB"

    String path = "src/main/test/resources/FCR.xmi"
    String cpModelPath = "src/main/test/resources/FCRCPModelWithSolution.xmi"


    Collection<IntVariableValueDTO> newConfiguration = new ArrayList<>()


    def setup() {
        NodeCandidate nodeCandidate = GroovyMock(NodeCandidate)
        nodeCandidate.getPrice() >> 10.0
        nodeCandidate.getNodeCandidateType() >> NodeCandidate.NodeCandidateTypeEnum.IAAS
        nodeCandidate.getId() >> "t1.micro"

        Hardware nodeCandidateHardware = GroovyMock(Hardware)
        nodeCandidateHardware.getCores() >> 2
        nodeCandidateHardware.getName() >> "t1.micro"
        nodeCandidateHardware.getRam() >> 1000
        nodeCandidateHardware.getDisk() >> 10
        nodeCandidate.getHardware() >> nodeCandidateHardware


        NodeCandidate nodeCandidate2 = GroovyMock(NodeCandidate)
        nodeCandidate2.getPrice() >> 10.0
        nodeCandidate2.getNodeCandidateType() >> NodeCandidate.NodeCandidateTypeEnum.IAAS
        nodeCandidate2.getId() >> "t1.micro"

        Hardware nodeCandidateHardware2 = GroovyMock(Hardware)
        nodeCandidateHardware2.getCores() >> 2
        nodeCandidateHardware2.getName() >> "t1.small"
        nodeCandidateHardware2.getRam() >> 1000
        nodeCandidateHardware2.getDisk() >> 10
        nodeCandidate2.getHardware() >> nodeCandidateHardware2

        List<NodeCandidate> list = new ArrayList<>()
        list.add(nodeCandidate)
        list.add(nodeCandidate2)
        Map<Integer, List<NodeCandidate>> nodeCandidatesMap = new HashMap<>()
        nodeCandidatesMap.put(1, list)
        mockNodeCandidates.getCheapest(_, 1, _) >> Optional.of(nodeCandidate2)
        mockNodeCandidates.getCheapest(_,0, _) >> Optional.of(nodeCandidate)
        mockNodeCandidates.getCheapest(_, _, _) >> Optional.of(nodeCandidate)
        mockNodeCandidates.get(_) >> nodeCandidatesMap

        utilityGeneratorProperties.setUtilityGenerator(new UtilityGeneratorProperties.UtilityGenerator())
        utilityGeneratorProperties.getUtilityGenerator().setDlmsControllerUrl("")

        jwtService = GroovyMock(JWTService)


        properties = GroovyMock(PenaltyFunctionProperties)
        Map<String, String> startupTimes = new HashMap<String, String>()
        startupTimes.put("t1.micro", "50")
        startupTimes.put("t1.small", "100")
        startupTimes.put("t1.xlarge", "120")
        startupTimes.put("t1.medium", "110")
        startupTimes.put("t1.xxlarge", "130")
        startupTimes.put("m1.tiny", "55")
        startupTimes.put("m1.small", "79")
        startupTimes.put("m1.medium", "88")
        startupTimes.put("m1.large", "132")
        startupTimes.put("m1.xlarge", "140")
        startupTimes.put("t1.large", "110")
        properties.getStartupTimes() >> startupTimes
        properties.getStateInfo() >>"1,0.6,0.5;1,1.7,160;4,7.5,850;8,15,1690;7,17.1,420;5,2,350;1,0.5,0.5;1,2.048,10;2,4.096,10;4,8.192,20;8,16.384,40"
        properties.getPort() >> 1234
        properties.getHost() >> "memcachehost"
    }

    def "FCR initial deployment"() {

        given:
        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))

        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, "src/main/test/resources/FCRCPModel.xmi", true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result != 0

    }

    def "FCRnew common reconfiguration test"() {

        given:

        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))

        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath, true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result != 0

    }

    def "FCRnew unmoveable component is moved"() {

        given:

        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))


        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath, true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result == 0

    }

    def "FCR without unmoveable component - test"() {

        given:
        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))

        path = "src/main/test/resources/FCRWithoutUnmoveable.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath,true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result != 0
    }

    def "FCR without optimisation requirement - test"() {

        given:
        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))
        newConfiguration.add(new IntVariableValueDTO("cardinality_Component_LB", 1, null))
        newConfiguration.add(new IntVariableValueDTO("provider_Component_LB", 2, null))


        path = "src/main/test/resources/FCRwithoutOptimisationRequirement.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath,true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result != 0
    }

    def "FCR with dlms utility - test"() {

        given:
        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 0, null))

        path = "src/main/test/resources/FCRwithDLMS.xmi"
//toupdate
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath, true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)

        then:
        noExceptionThrown()
        result != 0
    }


    def "FCR with penalty - test"() {

        given:
        newConfiguration.add(new IntVariableValueDTO(cardinalityName, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerName, 0, null))
        newConfiguration.add(new IntVariableValueDTO(dbCardinalityName, 1, null))
        newConfiguration.add(new IntVariableValueDTO(dbProviderName, 1, null))

        path = "src/main/test/resources/FCRwithPenalty.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpModelPath, true, mockNodeCandidates, utilityGeneratorProperties, securityProperties, jwtService, properties)

        when:
        double result = utilityGenerator.evaluate(newConfiguration)
        System.out.println("result = " + result)

        then:
        noExceptionThrown()
        result != 0
    }


}
