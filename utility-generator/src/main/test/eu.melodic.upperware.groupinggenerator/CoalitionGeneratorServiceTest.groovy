package eu.melodic.upperware.groupinggenerator

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.AllCoalitionsStructure
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.CoalitionGeneratorService
import spock.lang.Specification

class CoalitionGeneratorServiceTest extends Specification {

    def "two components"() {

        given:
        Collection<String> components = Arrays.asList("A", "B")
        CoalitionGeneratorService generator = new CoalitionGeneratorService()

        when:
        AllCoalitionsStructure result = generator.generateAllCoalitions(components)


        then:
        result.getCoalitions().size() == 2
        result.getCoalitions().get(1).getCoalitions().size() == 2
        result.getCoalitions().get(2).getCoalitions().size() == 1

    }

    def "three components"() {

        given:
        Collection<String> components = Arrays.asList("A", "B", "C")
        CoalitionGeneratorService generator = new CoalitionGeneratorService()

        when:
        AllCoalitionsStructure result = generator.generateAllCoalitions(components)


        then:
        result.getCoalitions().size() == 3
        result.getCoalitions().get(1).getCoalitions().size() == 3
        result.getCoalitions().get(2).getCoalitions().size() == 3
        result.getCoalitions().get(3).getCoalitions().size() == 1


    }

    def "four components"() {

        given:
        Collection<String> components = Arrays.asList("A", "B", "C", "D")
        CoalitionGeneratorService generator = new CoalitionGeneratorService()

        when:
        AllCoalitionsStructure result = generator.generateAllCoalitions(components)


        then:
        result.getCoalitions().size() == 4
        result.getCoalitions().get(1).getCoalitions().size() == 4
        result.getCoalitions().get(2).getCoalitions().size() == 6
        result.getCoalitions().get(3).getCoalitions().size() == 4
        result.getCoalitions().get(4).getCoalitions().size() == 1


    }
}
