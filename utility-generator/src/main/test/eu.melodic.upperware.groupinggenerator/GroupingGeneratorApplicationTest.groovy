package eu.melodic.upperware.groupinggenerator

import eu.melodic.cache.CacheService
import eu.melodic.cache.NodeCandidates
import eu.melodic.cache.impl.FilecacheService
import eu.melodic.upperware.penaltycalculator.PenaltyFunctionProperties
import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication
import eu.melodic.upperware.utilitygenerator.grouping_generator.GroupingGeneratorApplication
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.AllComponentsTogetherAlgorithm
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.EachComponentSeparatelyAlgorithm
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.RahwanAnyTimeAlgorithm
import eu.melodic.upperware.utilitygenerator.properties.UtilityGeneratorProperties
import eu.paasage.upperware.metamodel.cp.VariableType
import eu.paasage.upperware.security.authapi.properties.MelodicSecurityProperties
import eu.paasage.upperware.security.authapi.token.JWTService
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.IntVariableValueDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO
import io.github.cloudiator.rest.model.NodeCandidate
import spock.lang.Specification

class GroupingGeneratorApplicationTest extends Specification {


    GrouperData grouperData
    NodeCandidates mockNodeCandidates = GroovyMock(NodeCandidates)
    NodeCandidates samplerNodeCandidates
    CacheService<NodeCandidates> filecacheService = new FilecacheService();


    UtilityGeneratorProperties ugproperties = new UtilityGeneratorProperties()
    MelodicSecurityProperties securityProperties = new MelodicSecurityProperties()
    PenaltyFunctionProperties properties = new PenaltyFunctionProperties()

    JWTService jwtService

    Collection<VariableValueDTO> configuration = new ArrayList<>()
    Collection<IntVariableValueDTO> newConfiguration = new ArrayList<>()
    Collection<IntVariableValueDTO> secondConfiguration = new ArrayList<>()

    Collection<GroupingVariableDTO> groupedWithVariables = new ArrayList<>()
    Collection<GroupingVariableDTO> groupIdVariables = new ArrayList<>()
    List<Integer> groupIndicies = new LinkedList<>();

    Collection<VariableDTO> variables = new ArrayList<>()

    String cardinalityApp = "cardinality_Component_App"
    String cardinalityDB = "cardinality_Component_DB"
    String cardinalityLB = "cardinality_Component_LB"
    String providerApp = "provider_Component_App"
    String providerDB = "provider_Component_DB"
    String providerLB = "provider_Component_LB"
    String groupIdApp = "groupId_Component_App"
    String groupIdDB = "groupId_Component_DB"
    String groupIdLB = "groupId_Component_LB"
    String groupedWith_APP = "groupedWith_Component_App"
    String groupedWith_DB = "groupedWith_Component_DB"
    String groupedWith_LB = "groupedWith_Component_LB"


    def setup() {

        //samplerNodeCandidates = filecacheService.load("src/main/test/resources/FCRwithDLMSApp")
        samplerNodeCandidates = filecacheService.load("src/main/test/resources/grouping/FCRclear1585925784069")


        NodeCandidate nodeCandidate = GroovyMock(NodeCandidate)
        nodeCandidate.getPrice() >> 10.0
        nodeCandidate.getNodeCandidateType() >> NodeCandidate.NodeCandidateTypeEnum.IAAS

        NodeCandidate biggerNodeCandidate = GroovyMock(NodeCandidate)
        nodeCandidate.getPrice() >> 20.0
        nodeCandidate.getNodeCandidateType() >> NodeCandidate.NodeCandidateTypeEnum.IAAS

        List<NodeCandidate> list = new ArrayList<>()
        list.add(nodeCandidate)
        Map<Integer, List<NodeCandidate>> nodeCandidatesMap = new HashMap<>()
        nodeCandidatesMap.put(1, list)
        mockNodeCandidates.getCheapestWithAtLeast(_, _) >> Optional.of(nodeCandidate)
        mockNodeCandidates.get(_) >> nodeCandidatesMap

        ugproperties.setUtilityGenerator(new UtilityGeneratorProperties.UtilityGenerator())
        ugproperties.getUtilityGenerator().setDlmsControllerUrl("")

        newConfiguration.add(new IntVariableValueDTO(cardinalityApp, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerApp, 0, null))
        newConfiguration.add(new IntVariableValueDTO(cardinalityDB, 1, null))
        newConfiguration.add(new IntVariableValueDTO(providerDB, 0, null))


        secondConfiguration.add(new IntVariableValueDTO(cardinalityApp, 3, null))
        secondConfiguration.add(new IntVariableValueDTO(providerApp, 0, null))
        secondConfiguration.add(new IntVariableValueDTO(cardinalityDB, 3, null))
        secondConfiguration.add(new IntVariableValueDTO(providerDB, 0, null))


        variables.add(new VariableDTO(cardinalityDB, "Component_DB", VariableType.CARDINALITY))
        variables.add(new VariableDTO(providerDB, "Component_DB", VariableType.PROVIDER))
        variables.add(new VariableDTO(cardinalityApp, "Component_App", VariableType.CARDINALITY))
        variables.add(new VariableDTO(providerApp, "Component_App", VariableType.PROVIDER))



        groupIndicies.add(0)
        groupIndicies.add(1)


        groupIdVariables.add(new GroupingVariableDTO(groupIdApp, "Component_App", VariableType.GROUP_ID, groupIndicies))
        groupIdVariables.add(new GroupingVariableDTO(groupIdDB, "Component_DB", VariableType.GROUP_ID, groupIndicies))

        groupedWithVariables.add(new GroupingVariableDTO(groupedWith_APP, "Component_App", VariableType.GROUPED_WITH, groupIndicies))
        groupedWithVariables.add(new GroupingVariableDTO(groupedWith_DB, "Component_DB", VariableType.GROUPED_WITH, groupIndicies))

        configuration = newConfiguration
        jwtService = GroovyMock(JWTService)

        properties = GroovyMock(PenaltyFunctionProperties)
        Map<String, String> startupTimes = new HashMap<String, String>()
        properties.getStartupTimes() >> startupTimes
        properties.getStateInfo() >> "1,0.6,0.5;1,1.7,160;4,7.5,850;8,15,1690;7,17.1,420;5,2,350;1,0.5,0.5;1,2.048,10;2,4.096,10;4,8.192,20;8,16.384,40"
        properties.getPort() >> 1234
        properties.getHost() >> "memcachehost"

        grouperData = new GrouperData(variables, samplerNodeCandidates, groupIdVariables, groupedWithVariables, null)
    }

    def "return not null"() {

        given:

        String path = "src/main/test/resources/TwoComponentAppnewGrouping.xmi"
        String cpmodelPath = "src/main/test/resources/TwoComponentAppCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData)

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(newConfiguration)
        System.out.println("Result = " + result)

        then:

        result != null
        result.getUtilityValue() != 0.0
        noExceptionThrown()


    }


    def "each component separately"() {

        given:

        String path = "src/main/test/resources/TwoComponentAppnewGrouping.xmi"
        String cpmodelPath = "src/main/test/resources/TwoComponentAppCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new EachComponentSeparatelyAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(newConfiguration)
        System.out.println("Result = " + result)


        then:

        result != null
        result.getUtilityValue() == 0.5
        noExceptionThrown()


    }


    def "all components together - not valid partition"() {

        given:

        String path = "src/main/test/resources/TwoComponentAppnewGrouping.xmi"
        String cpmodelPath = "src/main/test/resources/TwoComponentAppCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new AllComponentsTogetherAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(newConfiguration)
        System.out.println("Result = " + result)


        then:

        result != null
        result.getUtilityValue() == 0.0
        noExceptionThrown()


    }

    def "all components together - valid partition"() {

        given:

        String path = "src/main/test/resources/TwoComponentAppnewGrouping.xmi"
        String cpmodelPath = "src/main/test/resources/TwoComponentAppCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new AllComponentsTogetherAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(secondConfiguration)
        System.out.println("Result = " + result)


        then:

        result != null
        result.getUtilityValue() > 0.0
        result.getUtilityValue() < 0.34
        noExceptionThrown()


    }


    def "rahwan anytime - valid partition"() {

        given:

        String path = "src/main/test/resources/TwoComponentAppnewGrouping.xmi"
        String cpmodelPath = "src/main/test/resources/TwoComponentAppCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new RahwanAnyTimeAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(secondConfiguration)
        System.out.println("Result = " + result)


        then:

        result != null
        result.getUtilityValue() != 0.0
        noExceptionThrown()


    }

    def "rahwan anytime - for FCR"() {

        given:

        newConfiguration.add(new IntVariableValueDTO(cardinalityLB, 2, null))
        newConfiguration.add(new IntVariableValueDTO(providerLB, 0, null))
        secondConfiguration.add(new IntVariableValueDTO(cardinalityLB, 3, null))
        secondConfiguration.add(new IntVariableValueDTO(providerLB, 0, null))

        groupIndicies.add(2)

        variables.add(new VariableDTO(cardinalityLB, "Component_LB", VariableType.CARDINALITY))
        variables.add(new VariableDTO(providerLB, "Component_LB", VariableType.PROVIDER))
        groupIdVariables.add(new GroupingVariableDTO(groupIdLB, "Component_LB", VariableType.GROUP_ID, groupIndicies))
        groupedWithVariables.add(new GroupingVariableDTO(groupedWith_LB, "Component_LB", VariableType.GROUPED_WITH, groupIndicies))

        grouperData = new GrouperData(variables, samplerNodeCandidates, groupIdVariables, groupedWithVariables, null)

        String path = "src/main/test/resources/FCR.xmi"
        String cpmodelPath = "src/main/test/resources/FCRCPModelGrouping.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, samplerNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new RahwanAnyTimeAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(secondConfiguration)
        System.out.println("Result = " + result)


        then:

        result != null
        //result.getUtilityValue() > 0.25
        result.getPartition().size() == 3
        noExceptionThrown()


    }

}
