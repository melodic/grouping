package eu.melodic.upperware.groupinggenerator

import com.opencsv.CSVReader
import eu.melodic.cache.CacheService
import eu.melodic.cache.NodeCandidates
import eu.melodic.cache.impl.FilecacheService
import eu.melodic.upperware.penaltycalculator.PenaltyFunctionProperties
import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication
import eu.melodic.upperware.utilitygenerator.grouping_generator.GroupingGeneratorApplication
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.RahwanAnyTimeAlgorithm
import eu.melodic.upperware.utilitygenerator.properties.UtilityGeneratorProperties
import eu.paasage.upperware.metamodel.cp.VariableType
import eu.paasage.upperware.security.authapi.properties.MelodicSecurityProperties
import eu.paasage.upperware.security.authapi.token.JWTService
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.IntVariableValueDTO
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO
import io.github.cloudiator.rest.model.NodeCandidate
import spock.lang.Specification

class GroupingPaperTest extends Specification{


    GrouperData grouperData
    NodeCandidates samplerNodeCandidates
    NodeCandidates limitedNodeCandidates
    CacheService<NodeCandidates> filecacheService = new FilecacheService();

    UtilityGeneratorProperties ugproperties = new UtilityGeneratorProperties()
    MelodicSecurityProperties securityProperties = new MelodicSecurityProperties()
    PenaltyFunctionProperties properties = new PenaltyFunctionProperties()

    JWTService jwtService



    def setup() {
        samplerNodeCandidates = filecacheService.load("src/main/test/resources/grouping/FCRclear1585925784069")

        Map<Integer, List<NodeCandidate>> candidatesForGrouper = new HashMap<>()

        for (Integer provider : samplerNodeCandidates.getCandidatesForGrouper().keySet()) {
            List<NodeCandidate> listNodeCandidates = samplerNodeCandidates.getCandidatesForGrouper().get(provider)
            List<NodeCandidate> newList = new ArrayList<>()
            for (NodeCandidate nc : listNodeCandidates) {
                if (nc.getPrice() < 1000) {
                    newList.add(nc)
                }
            }
            candidatesForGrouper.put(provider, newList)
        }
        limitedNodeCandidates = new NodeCandidates(samplerNodeCandidates.get(), candidatesForGrouper, Collections.emptyMap())
        limitedNodeCandidates.getCandidatesForGrouper()


        ugproperties.setUtilityGenerator(new UtilityGeneratorProperties.UtilityGenerator())
        ugproperties.getUtilityGenerator().setDlmsControllerUrl("")

        jwtService = GroovyMock(JWTService)

        properties = GroovyMock(PenaltyFunctionProperties)
        Map<String, String> startupTimes = new HashMap<String, String>()
        properties.getStartupTimes() >> startupTimes
        properties.getStateInfo() >> "1,0.6,0.5;1,1.7,160;4,7.5,850;8,15,1690;7,17.1,420;5,2,350;1,0.5,0.5;1,2.048,10;2,4.096,10;4,8.192,20;8,16.384,40"
        properties.getPort() >> 1234
        properties.getHost() >> "memcachehost"
    }


    def "first example with given solution space"(){

        given:

        String pathToSolutions = "fff"
        int numberOfVariables = 18
        String[] variablesNames = new String[numberOfVariables]
        List<Map<String,Integer>> solutions = new ArrayList<>();


        CSVReader reader = null;
        int counter = 0;
        try {
            reader = new CSVReader(new FileReader(pathToSolutions));
            String[] line;
            while ((line = reader.readNext()) != null) {
                if (counter == 0) {
                    for (int i; i++; i< line.size()) {
                        variablesNames[i] = line[i]
                    }
                }
                else {
                    for (int i; i++; i< line.size()) {
                        Map<String,Integer> map = new HashMap<>()
                        map.put(variablesNames[i],line[i])
                        variablesNames[i] = line[i]
                    }
                    //dodaj do mapy
                }
                System.out.println("Country [id= " + line[0] + ", code= " + line[1] + " , name=" + line[2] + "]");
            counter++
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    def "FCR with cores and ram - 6 components"() {

        given:

        Collection<String> components = Arrays.asList("Component_App", "Component_DB", "Component_LB",
                "Component_A", "Component_B", "Component_C")
        int counter = 0
        for (String componentName : components) {
            newConfiguration.add(new IntVariableValueDTO(cardinalityPrefix + componentName, 2, null))
            newConfiguration.add(new IntVariableValueDTO(providerPrefix + componentName, 0, null))
            newConfiguration.add(new IntVariableValueDTO(coresPrefix + componentName, 1, null))
            newConfiguration.add(new IntVariableValueDTO(ramPrefix + componentName, 1724, null))

            secondConfiguration.add(new IntVariableValueDTO(cardinalityPrefix + componentName, 3, null))
            secondConfiguration.add(new IntVariableValueDTO(providerPrefix + componentName, 0, null))
            secondConfiguration.add(new IntVariableValueDTO(coresPrefix + componentName, 2, null))
            secondConfiguration.add(new IntVariableValueDTO(ramPrefix + componentName, 40040, null))

            thirdConfiguration.add(new IntVariableValueDTO(cardinalityPrefix + componentName, 3, null))
            thirdConfiguration.add(new IntVariableValueDTO(providerPrefix + componentName, 0, null))
            thirdConfiguration.add(new IntVariableValueDTO(coresPrefix + componentName, 2, null))
            thirdConfiguration.add(new IntVariableValueDTO(ramPrefix + componentName, 400040, null))

            variables.add(new VariableDTO(cardinalityPrefix + componentName, componentName, VariableType.CARDINALITY))
            variables.add(new VariableDTO(providerPrefix + componentName, componentName, VariableType.PROVIDER))
            variables.add(new VariableDTO(coresPrefix + componentName, componentName, VariableType.CORES))
            variables.add(new VariableDTO(ramPrefix + componentName, componentName, VariableType.RAM))

            groupIndicies.add(counter)
            groupIdVariables.add(new GroupingVariableDTO(groupIdPrefix + componentName, componentName, VariableType.GROUP_ID, groupIndicies))
            groupedWithVariables.add(new GroupingVariableDTO(groupedWithPrefix + componentName, componentName, VariableType.GROUPED_WITH, groupIndicies))
            counter++
        }
        configuration = newConfiguration
        grouperData = new GrouperData(variables, samplerNodeCandidates, groupIdVariables, groupedWithVariables, null)

        String path = "src/main/test/resources/grouping/FCRnewGrouping6.xmi"
        String cpmodelPath = "src/main/test/resources/grouping/FCRclear1585925784069.xmi"
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(path, cpmodelPath, true, limitedNodeCandidates, ugproperties, securityProperties, jwtService, properties)
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new RahwanAnyTimeAlgorithm(grouperData))

        when:

        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(newConfiguration)
        GroupingAssignment result2 = groupingGeneratorApplication.findTheBestPartition(secondConfiguration)
        GroupingAssignment result3 = groupingGeneratorApplication.findTheBestPartition(thirdConfiguration)


        System.out.println("Result for 1GB RAM  = " + result)
        System.out.println("Result second, 60GB RAM = " + result2)
        System.out.println("Result for third, 600GB RAM = " + result3)



        then:
        //result.getUtilityValue() > 0.45
        //result.getUtilityValue() < 0.0

        noExceptionThrown()

    }




}
