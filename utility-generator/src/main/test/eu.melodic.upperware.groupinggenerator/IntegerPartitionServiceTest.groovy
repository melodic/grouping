package eu.melodic.upperware.groupinggenerator

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartition
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartitionSet
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.IntegerPartitionService
import spock.lang.Specification

class IntegerPartitionServiceTest extends Specification{


    def "integer partition test for 2"(){

        given:

        IntegerPartitionService integerPartitionService = new IntegerPartitionService()

        when:

        IntegerPartitionSet result = integerPartitionService.generateAllIntegerPartitions(2)


        then:

        result.getPartitions().contains(new IntegerPartition(Arrays.asList(2)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(1,1)))

    }


    def "integer partition test for 3"(){

        given:

        IntegerPartitionService integerPartitionService = new IntegerPartitionService()

        when:

        IntegerPartitionSet result = integerPartitionService.generateAllIntegerPartitions(3)


        then:

        result.getPartitions().contains(new IntegerPartition(Arrays.asList(3)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(2,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(1,1,1)))

    }


    def "integer partition test for 6"(){

        given:

        IntegerPartitionService integerPartitionService = new IntegerPartitionService()

        when:

        IntegerPartitionSet result = integerPartitionService.generateAllIntegerPartitions(6)


        then:

        result.getPartitions().contains(new IntegerPartition(Arrays.asList(6)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(5,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(4,2)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(4,1,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(3,3)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(3,2,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(3,1,1,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(2,2,2)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(2,2,1,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(2,1,1,1,1)))
        result.getPartitions().contains(new IntegerPartition(Arrays.asList(1,1,1,1,1,1)))
    }

}
