package eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class CoalitionElement {

    private String componentName;
}
