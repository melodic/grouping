package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import javafx.util.Pair;

public class Constraint extends Pair<Integer, Integer> {
    /**
     * Creates a new pair
     *
     * @param key   The key for this pair
     * @param value The value to use for this pair
     */
    public Constraint(Integer key, Integer value) {
        super(key, value);
    }
}
