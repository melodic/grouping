package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ConstraintGenerator {

    @Getter
    private List<Constraint> allPairs;

    public ConstraintGenerator(int size){
        this.allPairs = new LinkedList<>();
        for (int i=0; i<size; i++){
            for (int j=i+1; j<size; j++){
                allPairs.add(new Constraint(i,j));
            }
        }

    }

    public List<Constraint> pickRandomConstraints(int number){
        Collections.shuffle(allPairs);
        List<Constraint> chosenConstraints = new LinkedList<>();
        chosenConstraints.addAll(allPairs.subList(0,number));
        return chosenConstraints;
    }
}
