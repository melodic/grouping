package eu.melodic.upperware.utilitygenerator.grouping_generator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.Constraint;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GroupedWithGenerator {

    public static Collection<GroupingVariableDTO> createGroupedWithVariables(Collection<Constraint> constraints, Collection<String> componentNames) {
        Collection<GroupingVariableDTO> groupedWithVariables = new LinkedList<>();
        String groupedWithPrefix = "groupedWith_";
        int index = 0;
        for (String name : componentNames.stream().sorted().collect(Collectors.toList())) {
            List<Integer> allIndicies = IntStream.rangeClosed(0, componentNames.size()-1).boxed().collect(Collectors.toList());
            int finalIndex = index;
            List<Integer> possibleGroups = constraints.stream().filter(constraint -> constraint.getKey() == finalIndex).collect(Collectors.toList()).stream().map(Pair::getValue).collect(Collectors.toList());
            List<Integer> secondPossibleGroups = constraints.stream().filter(constraint -> constraint.getValue() == finalIndex).map(Pair::getKey).collect(Collectors.toList());
            possibleGroups.addAll(secondPossibleGroups);
            possibleGroups.add(finalIndex);
            allIndicies.removeAll(possibleGroups);

            GroupingVariableDTO var = new GroupingVariableDTO(groupedWithPrefix + name, name, VariableType.GROUPED_WITH, allIndicies);
            groupedWithVariables.add(var);
            index++;
        }

        //System.out.println("Grouped with variables: " + groupedWithVariables.toString());
        return groupedWithVariables;
    }


    private static Collection<GroupingVariableDTO> generateGroupedWithVariablesConstraints(Collection<String> components){
        Collection<GroupingVariableDTO> groupedWithVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();

        String groupedWithPrefix = "groupedWith_";


        /*
        v1: Component Name	Can be grouped with
        A	A	B	C	D
        B	A	B	C	D
        C	A	B	C	D
        D	A	B	C	D
        E	E


        */ /*
        GroupingVariableDTO groupingVariableDTO_A = new GroupingVariableDTO(groupedWithPrefix + "A", "A",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_B = new GroupingVariableDTO(groupedWithPrefix + "B", "B",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_C = new GroupingVariableDTO(groupedWithPrefix + "C", "C",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_D = new GroupingVariableDTO(groupedWithPrefix + "D", "D",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_E = new GroupingVariableDTO(groupedWithPrefix + "E", "E",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(4)));

        groupedWithVariables.add(groupingVariableDTO_A);
        groupedWithVariables.add(groupingVariableDTO_B);
        groupedWithVariables.add(groupingVariableDTO_C);
        groupedWithVariables.add(groupingVariableDTO_D);
        groupedWithVariables.add(groupingVariableDTO_E);

*/
        /*
        v2:
        Component Name	Can be grouped with
        A	A	C	D	E
        B	B	C	D	E
        C	A	B	C	D	E
        D	A	B	C	D	E
        E	A	B	C	D	E

        * */

        GroupingVariableDTO groupingVariableDTO_A = new GroupingVariableDTO(groupedWithPrefix + "A", "A",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 2 , 3, 4)));
        GroupingVariableDTO groupingVariableDTO_B = new GroupingVariableDTO(groupedWithPrefix + "B", "B",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(1, 2, 3, 4)));
        GroupingVariableDTO groupingVariableDTO_C = new GroupingVariableDTO(groupedWithPrefix + "C", "C",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3,4)));
        GroupingVariableDTO groupingVariableDTO_D = new GroupingVariableDTO(groupedWithPrefix + "D", "D",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3,4)));
        GroupingVariableDTO groupingVariableDTO_E = new GroupingVariableDTO(groupedWithPrefix + "E", "E",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1,2,3,4)));

        groupedWithVariables.add(groupingVariableDTO_A);
        groupedWithVariables.add(groupingVariableDTO_B);
        groupedWithVariables.add(groupingVariableDTO_C);
        groupedWithVariables.add(groupingVariableDTO_D);
        groupedWithVariables.add(groupingVariableDTO_E);


        return groupedWithVariables;
    }

    static Collection<GroupingVariableDTO> generateGroupIdVariables(Collection<String> components){
        Collection<GroupingVariableDTO> groupIdVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();
        String groupIdPrefix = "group_Id_";

        int counter = 0;
        for (String componentName : components) {
            groupIndicies.add(counter);
            groupIdVariables.add(new GroupingVariableDTO(groupIdPrefix + componentName, componentName, VariableType.GROUP_ID, groupIndicies));
            counter++;
        }
        //System.out.println("Group id variables: " + groupIdVariables.toString());
        return groupIdVariables;
    }

    static Collection<GroupingVariableDTO> generateGroupedWithVariables(Collection<String> components){
        Collection<GroupingVariableDTO> groupedWithVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();

        String groupedWithPrefix = "groupedWith_";

        int counter = 0;
        for (String componentName : components) {
            groupIndicies.add(counter);
            groupedWithVariables.add(new GroupingVariableDTO(groupedWithPrefix + componentName, componentName, VariableType.GROUPED_WITH, groupIndicies));
            //groupedWithVariables.add(new GroupingVariableDTO(groupedWithPrefix + componentName, componentName, VariableType.GROUPED_WITH, Collections.singletonList(counter)));
            counter++;
        }
        //System.out.println("Grouped with variables: " + groupedWithVariables.toString());
        return groupedWithVariables;
    }
}
