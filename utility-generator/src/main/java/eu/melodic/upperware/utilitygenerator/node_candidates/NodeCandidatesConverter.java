/* * Copyright (C) 2018 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.upperware.utilitygenerator.node_candidates;

import eu.melodic.cache.NodeCandidates;
import eu.melodic.upperware.utilitygenerator.cdo.camel_model.FromCamelModelExtractor;
import eu.melodic.upperware.utilitygenerator.evaluator.ConfigurationElement;
import eu.melodic.upperware.utilitygenerator.utility_function.ArgumentConverter;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import eu.passage.upperware.commons.model.tools.metadata.CamelMetadata;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mariuszgromada.math.mxparser.Argument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.node_candidates.NodeCandidateAttribute.createAttributeName;
import static eu.melodic.upperware.utilitygenerator.utility_function.ArgumentFactory.createArgument;
import static eu.passage.upperware.commons.model.tools.metadata.CamelMetadata.PRICE;
import static eu.passage.upperware.commons.model.tools.metadata.CamelMetadata.TOTAL_COST;
import static java.lang.String.format;

@Slf4j
public class NodeCandidatesConverter implements ArgumentConverter {

    private Collection<NodeCandidateAttribute> oneNodeCandidateAttributes;
    private Collection<NodeCandidateAttribute> allNodeCandidatesListAttributes;
    private Collection<NodeCandidateAttribute> currentConfigAttributes;
    @Getter
    private NodeCandidates nodeCandidates;
    private Collection<VariableDTO> variables;

    public NodeCandidatesConverter(FromCamelModelExtractor fromCamelModelExtractor, NodeCandidates nodeCandidates, Collection<VariableDTO> variablesFromConstraintProblem) {
        this.allNodeCandidatesListAttributes = fromCamelModelExtractor.getListOfAttributesOfNodeCandidates();
        this.currentConfigAttributes = fromCamelModelExtractor.getCurrentConfigAttributesOfNodeCandidates();
        this.nodeCandidates = nodeCandidates;
        this.variables = variablesFromConstraintProblem;
        this.oneNodeCandidateAttributes = fromCamelModelExtractor.getAttributesOfNodeCandidates();
        if (this.oneNodeCandidateAttributes.isEmpty()) {
            //this.oneNodeCandidateAttributes = createCostAttributesForAllComponents();
            this.oneNodeCandidateAttributes = createTotalCostAttribute();
        }
        if (CollectionUtils.isNotEmpty(allNodeCandidatesListAttributes)) {
            log.info("Attributes of list of Node Candidates: {}", allNodeCandidatesListAttributes);
            log.warn("Flag on candidates is not supported in Utility Generator");
        }
        log.info("Attributes of Node Candidates: {}", oneNodeCandidateAttributes);
    }

    @Override
    public Collection<Argument> convertToArguments(Collection<VariableValueDTO> solution, Collection<ConfigurationElement> newConfiguration) {
        return convertAttributes(this.oneNodeCandidateAttributes, newConfiguration);
    }

    public Collection<Argument> convertCurrentConfigAttributesOfNodeCandidates(Collection<ConfigurationElement> configuration) {
        if (configuration.isEmpty()) {
            return setDefaultValuesOfAttributes(this.currentConfigAttributes);
        }
        return convertAttributes(this.currentConfigAttributes, configuration);
    }

    private Collection<NodeCandidateAttribute> createTotalCostAttribute() {
        log.info("Creating default TotalCost attribute");
        NodeCandidateAttribute totalCost = new NodeCandidateAttribute(createAttributeName("Total", CamelMetadata.TOTAL_COST), "", TOTAL_COST, false);
        return Collections.singletonList(totalCost);
    }

    private Collection<NodeCandidateAttribute> createCostAttributesForAllComponents() {
        log.info("Creating default cost attributes for all components");
        return this.variables.stream()
                .filter(v -> VariableType.CARDINALITY.equals(v.getType()))
                .map(v -> new NodeCandidateAttribute(createAttributeName(v.getComponentId(), CamelMetadata.PRICE), v.getComponentId(), CamelMetadata.PRICE, false))
                .collect(Collectors.toList());
    }

    private Collection<Argument> setDefaultValuesOfAttributes(Collection<NodeCandidateAttribute> attributes) {
        log.info("It is the initial deployment. Setting values of attributes of Node Candidates to default values");
        return attributes.stream().map(a -> createArgument(a.getName(), 1.0)).collect(Collectors.toList());
    }

    private Collection<Argument> convertAttributes(Collection<NodeCandidateAttribute> nodeCandidateAttributes,
            Collection<ConfigurationElement> newConfiguration) {
        return nodeCandidateAttributes.stream()
                .map(a -> createArgument(a.getName(),
                        getAttributeValue(newConfiguration, a.getComponentId(), a.getType())))
                .collect(Collectors.toList());
    }

    private Number getAttributeValue(Collection<ConfigurationElement> newConfiguration, String componentId, CamelMetadata type) {
        if (PRICE.equals(type)) {
            return getPriceOfNodeCandidate(getNodeCandidate(newConfiguration, componentId));
        } else if (TOTAL_COST.equals(type)) {
            //log.debug("Using the total cost attribute. The cost of all used Node Candidates will be considered");
            Collection<ConfigurationElement> configurationElements = filterOneNodeCandidateForGroup(newConfiguration);
            //log.debug("Filtered collection: {}", configurationElements);
            return configurationElements.stream()
                    .mapToInt(element -> element.getCardinality() * getPriceOfNodeCandidate(element.getNodeCandidate()).intValue()).sum();
        } else {
            throw new IllegalArgumentException(format("Illegal type of Node Candidate attribute: %s", type));

        }
    }

    private NodeCandidate getNodeCandidate(Collection<ConfigurationElement> newConfiguration, String componentId) {
        return newConfiguration.stream()
                .filter(configurationElement -> configurationElement.getId().equals(componentId))
                .findAny()
                .orElseThrow(() -> new IllegalStateException(format("Configuration Element for component %s is not found", componentId)))
                .getNodeCandidate();
    }

    private Number getPriceOfNodeCandidate(NodeCandidate nodeCandidate) {
        if (NodeCandidate.NodeCandidateTypeEnum.FAAS.equals(nodeCandidate.getNodeCandidateType())) {
            return nodeCandidate.getPricePerInvocation();
        } else if (NodeCandidate.NodeCandidateTypeEnum.IAAS.equals(nodeCandidate.getNodeCandidateType())) {
            return nodeCandidate.getPrice();
        } else {
            throw new IllegalStateException(format("Type of Node Candidate: %s is not supported", nodeCandidate.getNodeCandidateType()));
        }
    }

    private Collection<ConfigurationElement> filterOneNodeCandidateForGroup(Collection<ConfigurationElement> configuration) {
        Collection<ConfigurationElement> filteredList = new ArrayList<>();
        configuration
                .forEach(element -> {
                    if (filteredList.stream()
                            .noneMatch(filteredElement -> element.getGroupId() == filteredElement.getGroupId())) {
                        filteredList.add(element);
                    }
                });
        return filteredList;
    }


}
