package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class VirtualMachine {

    private String name;
    private int cores;
    private int ram;
    //private String provider;
    private int price;
}
