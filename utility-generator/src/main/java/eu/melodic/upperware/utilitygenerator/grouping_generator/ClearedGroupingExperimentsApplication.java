package eu.melodic.upperware.utilitygenerator.grouping_generator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.Constraint;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ConstraintGenerator;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.NodeCandidatesTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.RahwanAnyTimeAlgorithm;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.GroupedWithGenerator.createGroupedWithVariables;
import static eu.melodic.upperware.utilitygenerator.grouping_generator.GroupedWithGenerator.generateGroupIdVariables;
import static eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable.REQ_TYPE.*;

@Getter
public class ClearedGroupingExperimentsApplication {

    @Setter
    private GrouperData grouperData;

    public static final boolean GENERATING_DATA_FOR_ODPIP=true;

    public static final String INIT_COALITION_VALUES_PATH = "/Users/mrozanska/Desktop/grouping-paper-data/4components/";
    public static String COALITION_VALUES_PATH = INIT_COALITION_VALUES_PATH;
    public static final String CONSTRAINTS_DEFINITIONS_FOLDER="/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/constraints/";
    public static String ODP_IP_FILE_NAME="newtest";
    public static int avCoalitions;


    public static void main(String[] args){

        ResourceRequirementsTable fourComponents = new ResourceRequirementsTable(PAPER);
        ResourceRequirementsTable smallSizeComponents = new ResourceRequirementsTable(SMALL);
        ResourceRequirementsTable mediumSizeComponents = new ResourceRequirementsTable(MEDIUM);
        ResourceRequirementsTable largeSizeComponents = new ResourceRequirementsTable(BIG);
        NodeCandidatesTable nodeCandidatesTablePaper = new NodeCandidatesTable();

        ResourceRequirementsTable resourceRequirements = mediumSizeComponents;

        Map<Integer,List<Integer>> availableCoalitions = new HashMap<>();

        Map<Integer, List<List<Constraint>>> allConstrainedCases = new HashMap<>();
        int numberOfComponents = resourceRequirements.getRequirementsList().keySet().size();
        ConstraintGenerator constraintGenerator = new ConstraintGenerator(numberOfComponents); //number of components
        int maxConstraintsNumber = ( numberOfComponents * (numberOfComponents-1))/2;
        for (int i=0; i<maxConstraintsNumber; i++){
            List<List<Constraint>> constraintsTheSameNumber = new LinkedList<>();
            availableCoalitions.put(i, new LinkedList<>());
            avCoalitions = 0;
            for (int j=0; j<100; j++){
                constraintsTheSameNumber.add(constraintGenerator.pickRandomConstraints(i));
            }
            //System.out.println("Inhibition constriants of size: " + i + " are: " + constraintsTheSameNumber.toString());
            allConstrainedCases.put(i, constraintsTheSameNumber);

            //SAVE TO FILE:
        }
        writeDataToCsv(allConstrainedCases, "/Users/mrozanska/Desktop/new10components-constraints");


        Map<Integer, List<GroupingAssignment>> results = new HashMap<>();
        Map<Integer, int[]> overallResults = new HashMap<>();
        Map<Integer, double[]> utilitiesofResults = new HashMap<>();

        for (int i: allConstrainedCases.keySet()){

            List<GroupingAssignment> resultsForGivenNumberConstraints = new LinkedList<>();
            int j=0;
            for (List<Constraint> constraints: allConstrainedCases.get(i)){
                ODP_IP_FILE_NAME = "/0"+i+"/"+j;
                GroupingAssignment result= runExperiment(resourceRequirements, nodeCandidatesTablePaper, createGroupedWithVariables(constraints, resourceRequirements.getRequirementsList().keySet()));
                availableCoalitions.get(i).add(avCoalitions);
                resultsForGivenNumberConstraints.add(result);
                j++;
            }

            /*List<GroupingAssignment> resultsForGivenNumberConstraints = allConstrainedCases.get(i)
                    .stream()
                    .map(constraints ->
                            {
                                ODP_IP_FILE_NAME = "/0"+i+"/"+allConstrainedCases.get(i).indexOf(constraints);
                                GroupingAssignment result= runExperiment(resourceRequirements, nodeCandidatesTablePaper, createGroupedWithVariables(constraints, resourceRequirements.getRequirementsList().keySet()));
                                availableCoalitions.get(i).add(avCoalitions);
                                return result;
                            }
                    )
                    .collect(Collectors.toList());*/

            results.put(i, resultsForGivenNumberConstraints);
            int[] counters = resultsForGivenNumberConstraints
                    .stream()
                    .mapToInt(GroupingAssignment::getCounterCSG)
                    .toArray();
            overallResults.put(i, counters);
            double[] utilities = resultsForGivenNumberConstraints
                    .stream()
                    .mapToDouble(GroupingAssignment::getUtilityValue)
                    .toArray();
            utilitiesofResults.put(i, utilities);
        }
        /*System.out.println("RESULTS for number of components: " + numberOfComponents);
        for (int i: overallResults.keySet()){
            System.out.println("for number of constraints " +i + "number of groupings checked : " + Arrays.toString(overallResults.get(i)));
        }*/

        writeResultsToCsv(overallResults, "/Users/mrozanska/Desktop/newresults");
        writeUtilityResultsToCsv(utilitiesofResults, "/Users/mrozanska/Desktop/newutillity-results");
        writeMapOfLists(availableCoalitions, "/Users/mrozanska/Desktop/newavailable-groups");



    }

    private static GroupingAssignment runExperiment(ResourceRequirementsTable resourceRequirementsTable, NodeCandidatesTable nodeCandidatesTable, Collection<GroupingVariableDTO> groupedWithVariables){


        Collection<GroupingVariableDTO> groupIdVariables = generateGroupIdVariables(resourceRequirementsTable.getRequirementsList().keySet());
        GrouperData grouperData = new GrouperData(resourceRequirementsTable, groupIdVariables, groupedWithVariables);
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(nodeCandidatesTable, grouperData, new RahwanAnyTimeAlgorithm(grouperData, resourceRequirementsTable));

        //Collection<Integer> checkedCsg = new ArrayList<>();


        GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(resourceRequirementsTable);

        //checkedCsg.add((int) result.getUtilityValue());

        return result;

    }




    private static void writeDataToCsv(Map<Integer, List<List<Constraint>>> data, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            // Write CSV header
            fileWriter.append("NumberOfConstraints, List Index, ComponentIndex1, ComponentIndex2");
            fileWriter.append("\n");

            // Write data to CSV file
            for (Map.Entry<Integer, List<List<Constraint>>> entry : data.entrySet()) {
                Integer key = entry.getKey();
                List<List<Constraint>> valueLists = entry.getValue();
                for (int i = 0; i < valueLists.size(); i++) {
                    List<Constraint> values = valueLists.get(i);
                    for (Constraint value : values) {
                        fileWriter.append(String.valueOf(key));
                        fileWriter.append(",");
                        fileWriter.append(String.valueOf(i));
                        fileWriter.append(",");
                        fileWriter.append(String.valueOf(value.getKey()));
                        fileWriter.append(",");
                        fileWriter.append(String.valueOf(value.getValue()));
                        fileWriter.append("\n");
                    }
                }
            }
            System.out.println("CSV file was created successfully!");
        } catch (IOException e) {
            System.out.println("An error occurred while writing data to CSV file.");
            e.printStackTrace();
        }
    }

    public static void writeResultsToCsv(Map<Integer, int[]> data, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            // Write CSV header
            fileWriter.append("NumberOfConstraints, GroupingCounter");
            fileWriter.append("\n");

            // Write data to CSV file
            for (Map.Entry<Integer, int[]> entry : data.entrySet()) {
                Integer key = entry.getKey();
                int[] values = entry.getValue();
                StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(key));
                sb.append(",");
                for (int i = 0; i < values.length; i++) {
                    sb.append(String.valueOf(values[i]));
                    if (i != values.length - 1) {
                        sb.append(" ");
                    }
                }
                fileWriter.append(sb.toString());
                fileWriter.append("\n");
            }
            System.out.println("CSV file was created successfully!");
        } catch (IOException e) {
            System.out.println("An error occurred while writing data to CSV file.");
            e.printStackTrace();
        }
    }

    public static void writeUtilityResultsToCsv(Map<Integer, double[]> data, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            // Write CSV header
            fileWriter.append("NumberOfConstraints, GroupingCounter");
            fileWriter.append("\n");

            // Write data to CSV file
            for (Map.Entry<Integer, double[]> entry : data.entrySet()) {
                Integer key = entry.getKey();
                double[] values = entry.getValue();
                StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(key));
                sb.append(",");
                for (int i = 0; i < values.length; i++) {
                    sb.append(String.valueOf(values[i]));
                    if (i != values.length - 1) {
                        sb.append(" ");
                    }
                }
                fileWriter.append(sb.toString());
                fileWriter.append("\n");
            }
            System.out.println("CSV file was created successfully!");
        } catch (IOException e) {
            System.out.println("An error occurred while writing data to CSV file.");
            e.printStackTrace();
        }
    }

    public static void writeMapOfLists(Map<Integer, List<Integer>> map, String filename) {
        try (FileWriter writer = new FileWriter(filename)) {
            // Write header row
            writer.append("numberofconstraints,number of groups\n");

            // Write data rows
            for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
                int key = entry.getKey();
                List<Integer> values = entry.getValue();
                for (int value : values) {
                    writer.append(Integer.toString(key))
                            .append(",")
                            .append(Integer.toString(value))
                            .append("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
