package eu.melodic.upperware.utilitygenerator.utility_function;

import eu.melodic.upperware.utilitygenerator.evaluator.ConfigurationElement;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import org.mariuszgromada.math.mxparser.Argument;

import java.util.Collection;

public interface ArgumentConverter {

    Collection<Argument> convertToArguments(Collection<VariableValueDTO> solution, Collection<ConfigurationElement> newConfiguration);
}
