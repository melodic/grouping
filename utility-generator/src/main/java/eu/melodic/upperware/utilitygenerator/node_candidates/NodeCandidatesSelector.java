package eu.melodic.upperware.utilitygenerator.node_candidates;

import eu.melodic.cache.NodeCandidates;
import eu.melodic.cache.node_candidates_predicates.single_predicate_factory.Impl.*;
import eu.melodic.upperware.utilitygenerator.evaluator.ConfigurationElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils.*;
import static java.lang.String.format;
import static java.util.Objects.isNull;

@Slf4j
public class NodeCandidatesSelector {

    private NodeCandidates nodeCandidates;
    @Getter
    private double maxPrice;
    @Getter
    private double minPrice;

    public NodeCandidatesSelector(NodeCandidates nodeCandidates) {
        this.nodeCandidates = nodeCandidates;
        this.maxPrice = nodeCandidates
                .getCandidatesForGrouper()
                .values()
                .stream()
                .mapToDouble(ncList -> ncList.stream()
                        .mapToDouble(NodeCandidate::getPrice)
                        .max()
                        .orElseThrow(() -> new IllegalStateException("There was an error during looking for the maximum price of Node Candidate in the list")))
                .max()
                .orElseThrow(() -> new IllegalStateException("There was an error during looking for the maximum price of Node Candidate"));
        this.maxPrice -= 20000; //fixme
        this.minPrice = nodeCandidates
                .getCandidatesForGrouper()
                .values()
                .stream()
                .mapToDouble(ncList -> ncList.stream()
                        .mapToDouble(NodeCandidate::getPrice)
                        .min()
                        .orElseThrow(() -> new IllegalStateException("There was an error during looking for the maximum price of Node Candidate in the list")))
                .min()
                .orElseThrow(() -> new IllegalStateException("There was an error during looking for the maximum price of Node Candidate"));
        log.info("Prices of Node Candidates: minimum: {}, maximum: {} ", minPrice, maxPrice);
    }

    public NodeCandidate getNodeCandidateForGroup(Collection<PartitionElement> elements, Collection<VariableValueDTO> solution, Collection<VariableDTO> variables) {
        //assumption: constraints are checked

        List<String> componentNames = elements.stream()
                .map(PartitionElement::getComponentName)
                .collect(Collectors.toList());

        int providerId = getProviderValue(componentNames.get(0), variables, solution);

        Predicate<NodeCandidate>[] nodeCandidatePredicates = makePredicatesFromSolutionForGroup(solution, componentNames, variables);

        //log.debug(Arrays.stream(nodeCandidatePredicates)
         //       .map(Object::toString)
         //       .collect(Collectors.joining(",", "Filtering node candidates by components " + componentNames.toString() + ", provider with id: " + providerId + " and " + nodeCandidatePredicates.length + " predicates [", "]")));

        NodeCandidate nodeCandidate = nodeCandidates.getCheapestForGroup(providerId, nodeCandidatePredicates)
                .orElse(null);
        //.orElseThrow(() -> new CacheException(String.format("Could not find cheapest nodeCandidate for components %s, provider with index %d and %d predicates", componentNames.toString(), providerId, nodeCandidatePredicates.length)));

        //log.debug("Found Node Candidate: {}", nodeCandidate);
        return nodeCandidate;
    }


    public static Collection<ConfigurationElement> convertSolutionToNodeCandidates(Collection<VariableDTO> variables, NodeCandidates nodeCandidates, Collection<VariableValueDTO> solution) {
        //log.debug("Converting solution to Node Candidates");

        Collection<ConfigurationElement> newConfiguration = new ArrayList<>();
        Map<String, Integer> cardinalitiesForComponent = getCardinalitiesForComponent(solution, variables);

        for (String componentId : cardinalitiesForComponent.keySet()) {
            //log.debug("Converting solution for component {}", componentId);
            int provider = getProviderValue(componentId, variables, solution);
            Predicate<NodeCandidate>[] requirementsForComponent = makePredicatesFromSolution(componentId, solution, variables);
            NodeCandidate theCheapest = nodeCandidates.getCheapestForSingleComponent(componentId, provider, requirementsForComponent).orElse(null);

            if (isNull(theCheapest)) {
                //log.debug("Node Candidates for component {} with provider {} is not found", componentId, provider);
                return Collections.emptyList();
            }
            //log.debug("Got the cheapest Node Candidate from component {} with provider {}", componentId, provider);

            newConfiguration.add(new ConfigurationElement(componentId, theCheapest, cardinalitiesForComponent.get(componentId)));
        }
        return newConfiguration;
    }

    public static Collection<ConfigurationElement> convertDeployedSolutionToNodeCandidates(Collection<VariableDTO> variables, NodeCandidates nodeCandidates, Collection<VariableValueDTO> solution) {
        if (solution == null) {
            return Collections.emptyList();
        }
        return convertSolutionToNodeCandidates(variables, nodeCandidates, solution);
    }

    public static VariableValueDTO getVariableValue(Collection<VariableValueDTO> solution, VariableDTO variable) {

        return CollectionUtils.emptyIfNull(solution).stream()
                .filter(variableValue -> variable.getId().equals(variableValue.getName()))
                .findAny().orElseThrow(() -> new IllegalStateException(format("Variable %s does not exist", variable.getId())));
    }

    private static Predicate<NodeCandidate>[] makePredicatesFromSolution(String componentId, Collection<VariableValueDTO> solution, Collection<VariableDTO> variables) {
        Collection<String> variableNamesForComponent = getVariablesForComponent(componentId, variables);

        List<VariableValueDTO> variablesForComponent = solution.stream()
                .filter(var -> variableNamesForComponent.contains(var.getName()))
                .collect(Collectors.toList());

        List<Predicate<NodeCandidate>> predicates = new ArrayList<>();

        for (VariableValueDTO var : variablesForComponent) {
            VariableType type = getVariableType(var.getName(), variables);

            switch (type) {
                case RAM:
                    //log.debug("Creating getRamPredicate for value {}", var.getValue());
                    predicates.add(RamPredicateFactory.getRamPredicate((long) (int) var.getValue()));
                    break;
                case CORES:
                    //log.debug("Creating getCoresPredicate for value {}", var.getValue());
                    predicates.add(CoresPredicateFactory.getCoresPredicate((int) var.getValue()));
                    break;
                case OS:
                    //log.debug("Creating getOsPredicate for value {}", var.getValue());
                    predicates.add(OsPredicateFactory.getOsPredicate((int) var.getValue()));
                    break;
                case STORAGE:
                    predicates.add(StoragePredicateFactory.getStoragePredicate((int) var.getValue()));
                    break;
                case LATITUDE:
                    //log.debug("Creating getLatitudePredicate for value {}", var.getValue());
                    predicates.add(LatitudePredicateFactory.getLatitudePredicate((int) var.getValue()));
                    break;
                case LONGITUDE:
                    //log.debug("Creating getLongitudePredicate for value {}", var.getValue());
                    predicates.add(LongitudePredicateFactory.getLongitudePredicate((int) var.getValue()));
                    break;
                case LOCATION:
                case CARDINALITY:
                case PROVIDER:
                case CPU:
                    break;
            }
        }

        return predicates.toArray(new Predicate[predicates.size()]);
    }

    private static Predicate<NodeCandidate>[] makePredicatesFromSolutionForGroup(Collection<VariableValueDTO> solution, Collection<String> componentNames, Collection<VariableDTO> variables) {
        //for each component in the group

        Collection<String> variableNamesForComponents = getVariableForComponents(componentNames, variables);

        List<VariableValueDTO> variablesForComponents = solution.stream()
                .filter(var -> variableNamesForComponents.contains(var.getName()))
                .collect(Collectors.toList());


        List<Predicate<NodeCandidate>> predicates = new ArrayList<>();

        List<VariableValueDTO> coresVariableValues = getVariableValues(variablesForComponents, variables, VariableType.CORES);
        if (CollectionUtils.isNotEmpty(coresVariableValues)) {
            coresVariableValues
                    .stream()
                    .mapToInt(variableValue -> variableValue.getValue().intValue())
                    .max()
                    .ifPresent(value -> predicates.add(CoresPredicateFactory.getCoresPredicateAtLeast(value)));
        }

        List<VariableValueDTO> ramVariableValues = getVariableValues(variablesForComponents, variables, VariableType.RAM);
        if (CollectionUtils.isNotEmpty(ramVariableValues)) {
            long ramValue = ramVariableValues
                    .stream()
                    .mapToLong(variableValue -> (long) (int) variableValue.getValue().intValue())
                    .sum();
            predicates.add(RamPredicateFactory.getRamPredicateAtLeast((long) (int) ramValue));
        }

        List<VariableValueDTO> storageVariableValues = getVariableValues(variablesForComponents, variables, VariableType.STORAGE);
        if (CollectionUtils.isNotEmpty(storageVariableValues)) {
            int storageValue = storageVariableValues
                    .stream()
                    .mapToInt(variableValue -> variableValue.getValue().intValue())
                    .sum();
            predicates.add(StoragePredicateFactory.getStoragePredicateAtLeast(storageValue));
        }
            //todo: check if os are equal for all
            getVariableValues(variablesForComponents, variables, VariableType.OS)
                    .stream()
                    .mapToInt(variableValue -> variableValue.getValue().intValue())
                    .findAny()
                    .ifPresent(value -> predicates.add(OsPredicateFactory.getOsPredicate(value)));


            //todo: check if latitude are equal for all
            getVariableValues(variablesForComponents, variables, VariableType.LATITUDE)
                    .stream()
                    .findAny()
                    .ifPresent(variableValue -> predicates.add(LatitudePredicateFactory.getLatitudePredicate(variableValue.getValue().intValue())));


            //todo: check if longitude are equal for all
            getVariableValues(variablesForComponents, variables, VariableType.LONGITUDE)
                    .stream()
                    .findAny()
                    .ifPresent(variableValue -> predicates.add(LongitudePredicateFactory.getLongitudePredicate(variableValue.getValue().intValue())));

            return predicates.toArray(new Predicate[predicates.size()]);
    }
}
