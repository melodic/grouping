package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@EqualsAndHashCode
@ToString
@Setter
public class IntegerPartition {

    @Setter
    private List<Integer> pattern; //todo: partition
    private double maximum;
    private double average;

    public IntegerPartition(List<Integer> pattern){
        this.pattern = pattern;
        this.maximum = Double.NEGATIVE_INFINITY;
        this.average = Double.NEGATIVE_INFINITY;
    }
    public IntegerPartition() {
        this.pattern = new ArrayList<>();
    }

}
