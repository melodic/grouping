package eu.melodic.upperware.utilitygenerator.grouping_generator;

import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.NodeCandidatesTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.ConstraintChecker;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorServiceCleared;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorServiceImpl;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.EachComponentSeparatelyAlgorithm;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.PartitionService;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;

@Slf4j
@AllArgsConstructor
public class GroupingGeneratorApplication {

    private PartitionService partitionService;
    private PartitionEvaluatorService partitionEvaluatorService;


    public GroupingGeneratorApplication(UtilityGeneratorApplication utilityGeneratorApplication, GrouperData data) {
        log.info("Creating Grouping Generator Application");
        this.partitionEvaluatorService = new PartitionEvaluatorServiceImpl(utilityGeneratorApplication, data);
        log.info("Creating default policy for partitioning: each component separately");
        this.partitionService = new EachComponentSeparatelyAlgorithm(data);
    }

    public GroupingGeneratorApplication(NodeCandidatesTable nodeCandidatesTable, GrouperData data, PartitionService partitionService){

        this.partitionEvaluatorService = new PartitionEvaluatorServiceCleared(nodeCandidatesTable, new ConstraintChecker(data));
        this.partitionService = partitionService;
    }


    public GroupingGeneratorApplication(UtilityGeneratorApplication utilityGeneratorApplication, GrouperData data, PartitionService partitionService) {
        log.info("Creating Grouping Generator Application with chosen partition service: {}", partitionService);
        this.partitionEvaluatorService = new PartitionEvaluatorServiceImpl(utilityGeneratorApplication, data);
        this.partitionService = partitionService;
    }


    public GroupingAssignment findTheBestPartition(Collection<VariableValueDTO> solution) {
        //log.info("Finding the best partition for solution: {}", solution.toString());
        return partitionService.findBestPartition(solution, partitionEvaluatorService);
    }

    public GroupingAssignment findTheBestPartition(ResourceRequirementsTable resourceRequirementsTable){
        return partitionService.findBestPartition(resourceRequirementsTable, partitionEvaluatorService);
    }
}
