package eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
public class CoalitionList {

    private List<Coalition> coalitions;
    @Setter
    private double maxValue;
    @Setter
    private double avgValue;

    public CoalitionList(List<Coalition> coalitions) {
        this.coalitions = coalitions;
    }

    @Override
    public String toString() {
        return "maxValue: " + maxValue + ", avgValue: " + avgValue
                + ", coalitions: " + coalitions.toString() + "\n";
    }

}
