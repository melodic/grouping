package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Getter
@AllArgsConstructor
@Slf4j
@ToString
public class IntegerPartitionSet {


    /* pattern, e.g.
     *  6
     *  5 1
     *  4 2
     *  4 1 1
     *  3 3
     *  3 2 1
     *  3 1 1 1
     *  2 2 2
     *  2 2 1 1
     *  2 1 1 1 1
     *  1 1 1 1 1 1
     */
    @Setter
    private List<IntegerPartition> partitions;
    private int number;


    public IntegerPartitionSet(int number) {
        this.number = number;
        this.partitions = new ArrayList<>();
    }

    public static IntegerPartitionSet difference(IntegerPartitionSet subtrahend, IntegerPartitionSet minuend) {
        //log.debug("Making a difference between: {}, and {}", subtrahend.getPartitions(), minuend.getPartitions());
        if (subtrahend.getNumber() != minuend.getNumber()) {
            throw new IllegalStateException("Cannot make difference between two integer partition sets");
        }
        List<IntegerPartition> difference = subtrahend.partitions.stream()
                .filter(partition -> !minuend.getPartitions().contains(partition))
                .collect(Collectors.toList());
        return new IntegerPartitionSet(difference, subtrahend.getNumber());
    }


    public double calculateMaximum() {
        return this.getPartitions().stream().mapToDouble(IntegerPartition::getMaximum).max().orElse(Double.MIN_VALUE);
    }

    public double calculateAverage() {
        return this.getPartitions().stream().mapToDouble(IntegerPartition::getAverage).average().orElse(Double.MIN_VALUE);

    }

}
