package eu.melodic.upperware.utilitygenerator.grouping_generator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.AllCoalitionsStructure;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionList;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoalitionsConverter {

    @Getter
    private ArrayList<Coalition> coalitions;

    public CoalitionsConverter(AllCoalitionsStructure allCoalitions, List<String> componentNames){

        ArrayList<String> binaryNumbers = new ArrayList<>();

        long allCoalitionsSize = allCoalitions.getCoalitions().values()
                .stream()
                .mapToInt(coalitionList -> coalitionList.getCoalitions().size())
                .sum();

        for (int i=0; i<=allCoalitionsSize; i++){
            binaryNumbers.add(Integer.toBinaryString(i));
        }

        AllCoalitionsStructure normalizedCoalitions = normalizeCoalitionValues(allCoalitions);

        ArrayList<Coalition> coalitionsOrdered = new ArrayList<>();
        for (String binaryNumber: binaryNumbers){
            List<String> componentsInCoalition = new ArrayList<>();
            for (int i=0; i<binaryNumber.length(); i++){//was components.size
                char c = binaryNumber.charAt(i);

                if (Character.getNumericValue(binaryNumber.charAt(i))==1){
                    int index = componentNames.size() - binaryNumber.length() + i;
                    componentsInCoalition.add(componentNames.get(componentNames.size()-1-index));

                }
            }
            if (componentsInCoalition.size() > 0){
                coalitionsOrdered
                        .add(normalizedCoalitions.getCoalitions().get(componentsInCoalition.size())
                                .getCoalitions()
                                .stream()
                                .filter(coalition -> coalition.getComponentNames().containsAll(componentsInCoalition))
                                .findFirst()
                                .orElseThrow(()-> new IllegalStateException(
                                        "Not found coalition for set: " + componentsInCoalition.toString()
                                                + " this should be negative infity")));
            }
        }

        this.coalitions = coalitionsOrdered;

    }

    public AllCoalitionsStructure normalizeCoalitionValues(AllCoalitionsStructure allCoalitionsStructure){
        Map<Integer, CoalitionList> normalizedCoalitions = new HashMap<>();

        //zamien negative infinity na 0
        allCoalitionsStructure.getCoalitions().values()
                .stream()
                .forEach(coalitionList -> coalitionList
                        .getCoalitions()
                        .stream()
                        .filter(coalition -> coalition.getCoalitionValue()==Double.NEGATIVE_INFINITY)
                        .forEach(coalition -> coalition.setCoalitionValue(0.0)));


        //znajdz taka z najwieksza wartoscia

        double maxCoalitionValue = allCoalitionsStructure.getCoalitions().values()
                .stream()
                .mapToDouble(coalitionList -> coalitionList.getCoalitions().stream().mapToDouble(Coalition::getCoalitionValue).min().getAsDouble())
                .min().getAsDouble();

        allCoalitionsStructure.getCoalitions()
                .keySet()
                .forEach(size -> {
                    List<Coalition> normalizedCoalitionsForSize = new ArrayList<>();
                    allCoalitionsStructure.getCoalitions().get(size).getCoalitions()
                            .forEach(coalition -> normalizedCoalitionsForSize.add(
                                            new Coalition(coalition.getElements(),
                                                    normalizeCoalitionValue(maxCoalitionValue, size, coalition.getCoalitionValue()))));
                    normalizedCoalitions.put(size, new CoalitionList(normalizedCoalitionsForSize));
                });

        return new AllCoalitionsStructure(normalizedCoalitions);
    }

    private double normalizeCoalitionValue(double minimum, int coalitionSize, double value){
        if (value == 0){
            return 0.0;
        }
        //wersja 1:double normalized = coalitionSize * (-1) * minimum + value;

        double normalized = coalitionSize * (-1) * minimum *2 + value;

        //System.out.println("changing: " + value + "to " + normalized + " size of coalition: " + coalitionSize);
        return normalized;
    }

}
