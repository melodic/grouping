package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class ResourceRequirementsTable {



    private Map<String, ResourceRequirements> requirementsList;

    public enum REQ_TYPE  {PAPER, SMALL, MEDIUM, BIG};


    public ResourceRequirementsTable(REQ_TYPE type){

        if (type == REQ_TYPE.PAPER){
            ResourceRequirements cA = new ResourceRequirements("A", 6, 10);
            ResourceRequirements cB = new ResourceRequirements("B", 2, 4);
            ResourceRequirements cC = new ResourceRequirements("C", 1, 3);
            ResourceRequirements cD = new ResourceRequirements("D", 1, 2);
            requirementsList = new HashMap<>();
            requirementsList.put(cA.getComponentName(), cA);
            requirementsList.put(cB.getComponentName(), cB);
            requirementsList.put(cC.getComponentName(), cC);
            requirementsList.put(cD.getComponentName(), cD);
        }
        else if (type == REQ_TYPE.SMALL){
            ResourceRequirements cA = new ResourceRequirements("A", 6, 10);
            ResourceRequirements cB = new ResourceRequirements("B", 2, 4);
            ResourceRequirements cC = new ResourceRequirements("C", 1, 3);
            ResourceRequirements cD = new ResourceRequirements("D", 1, 2);
            ResourceRequirements cE = new ResourceRequirements("E", 3, 2);

            requirementsList = new HashMap<>();
            requirementsList.put(cA.getComponentName(), cA);
            requirementsList.put(cB.getComponentName(), cB);
            requirementsList.put(cC.getComponentName(), cC);
            requirementsList.put(cD.getComponentName(), cD);
            requirementsList.put(cE.getComponentName(), cE);
        }
        else if (type == REQ_TYPE.MEDIUM){
            ResourceRequirements cA = new ResourceRequirements("A", 1, 1);
            ResourceRequirements cB = new ResourceRequirements("B", 1, 2);
            ResourceRequirements cC = new ResourceRequirements("C", 1, 2);
            ResourceRequirements cD = new ResourceRequirements("D", 1, 3);
            ResourceRequirements cE = new ResourceRequirements("E", 2, 2);
            ResourceRequirements cF = new ResourceRequirements("F", 2, 3);
            ResourceRequirements cG = new ResourceRequirements("G", 2, 4);
            ResourceRequirements cH = new ResourceRequirements("H", 3, 3);
            ResourceRequirements cI = new ResourceRequirements("I", 3, 4);
            ResourceRequirements cJ = new ResourceRequirements("J", 3, 6);
            requirementsList = new HashMap<>();
            requirementsList.put(cA.getComponentName(), cA);
            requirementsList.put(cB.getComponentName(), cB);
            requirementsList.put(cC.getComponentName(), cC);
            requirementsList.put(cD.getComponentName(), cD);
            requirementsList.put(cE.getComponentName(), cE);
            requirementsList.put(cF.getComponentName(), cF);
            requirementsList.put(cG.getComponentName(), cG);
            requirementsList.put(cH.getComponentName(), cH);
            requirementsList.put(cI.getComponentName(), cI);
            requirementsList.put(cJ.getComponentName(), cJ);
        }
        else if (type == REQ_TYPE.BIG){
            ResourceRequirements cA = new ResourceRequirements("A", 6, 10);
            ResourceRequirements cB = new ResourceRequirements("B", 2, 4);
            ResourceRequirements cC = new ResourceRequirements("C", 1, 3);
            ResourceRequirements cD = new ResourceRequirements("D", 1, 2);
            ResourceRequirements cE = new ResourceRequirements("E", 3, 2);
            ResourceRequirements cF = new ResourceRequirements("F", 7, 18);
            ResourceRequirements cG = new ResourceRequirements("G", 1, 2);
            ResourceRequirements cH = new ResourceRequirements("H", 12, 20);
            ResourceRequirements cI = new ResourceRequirements("I", 6, 14);
            ResourceRequirements cJ = new ResourceRequirements("J", 2, 2);
            requirementsList = new HashMap<>();
            requirementsList.put(cA.getComponentName(), cA);
            requirementsList.put(cB.getComponentName(), cB);
            requirementsList.put(cC.getComponentName(), cC);
            requirementsList.put(cD.getComponentName(), cD);
            requirementsList.put(cE.getComponentName(), cE);
            requirementsList.put(cF.getComponentName(), cF);
            requirementsList.put(cG.getComponentName(), cG);
            requirementsList.put(cH.getComponentName(), cH);
            requirementsList.put(cI.getComponentName(), cI);
            requirementsList.put(cJ.getComponentName(), cJ);
        }


    }
}
