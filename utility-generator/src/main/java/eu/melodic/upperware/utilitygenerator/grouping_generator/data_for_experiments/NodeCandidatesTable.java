package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class NodeCandidatesTable {


    private List<VirtualMachine> availableNodeCandidates;
    @Getter
    private int MAX_PRICE;
    @Getter
    private int MIN_PRICE;

    public NodeCandidatesTable(){
        VirtualMachine vm1 = new VirtualMachine("VM1", 1, 2, 2);
        VirtualMachine vm2 = new VirtualMachine("VM2", 2, 4, 4);
        VirtualMachine vm3 = new VirtualMachine("VM3", 4, 8, 8);
        VirtualMachine vm4 = new VirtualMachine("VM4", 8, 16, 16);
        VirtualMachine vm5 = new VirtualMachine("VM5", 16, 32, 32);
        availableNodeCandidates = new ArrayList<>();
        availableNodeCandidates.add(vm1);
        availableNodeCandidates.add(vm2);
        availableNodeCandidates.add(vm3);
        availableNodeCandidates.add(vm4);
        availableNodeCandidates.add(vm5);
        MAX_PRICE = 32;
        MIN_PRICE = 2;
    }

    public Optional<VirtualMachine> findCheapestVM(ResourceRequirementsTable resourceRequirementsTable, Coalition coalition){
        //take the component names from coalition, then sum resource requirements
        Map<String, ResourceRequirements> requirementsList = resourceRequirementsTable.getRequirementsList();
        int ramReq =0;
        int coresReq = 0;
        for (String componentName: requirementsList.keySet()){
            if (coalition.getComponentNames().contains(componentName)){
                ramReq += requirementsList.get(componentName).getRam();
                coresReq += requirementsList.get(componentName).getCores();
            }
        }
        //System.out.println("Looking for the cheapest VM for group:" + coalition.getComponentNames() + "with requirements: RAM: " + ramReq + " CORES: " + coresReq);
        return findCheapestVM(coresReq, ramReq);

    }

    private Optional<VirtualMachine> findCheapestVM(int cores, int ram){
        List<VirtualMachine> availableVMs = availableNodeCandidates.stream().filter(vm -> (cores <= vm.getCores() && ram <= vm.getRam())).collect(Collectors.toList());
        if (availableVMs.isEmpty()){
            //System.out.println("No VM for requirements: CORES: " + cores + " RAM: " + ram);
            return Optional.empty();
        }
        else{
            VirtualMachine cheapestVM = availableVMs.get(0); //if we are here it means that at lest one exist
            for (VirtualMachine vm : availableVMs){
                if (vm.getPrice() < cheapestVM.getPrice()){
                    cheapestVM = vm;
                }
            }

            //System.out.println("Cheapest VM found: " + cheapestVM.getName() + ", price= " + cheapestVM.getPrice());
            return Optional.of(cheapestVM);
        }
    }
}
