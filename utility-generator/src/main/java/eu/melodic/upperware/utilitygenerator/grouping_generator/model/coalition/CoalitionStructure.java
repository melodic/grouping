package eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@ToString
public class CoalitionStructure {

    private double value;
    private Collection<Coalition> structure;

    public CoalitionStructure(){
        this.value = Double.NEGATIVE_INFINITY;
        this.structure = new ArrayList<>();
    }

    public CoalitionStructure(Collection<Coalition> structure){
        this.structure = structure;
        if (structure.isEmpty()){
            this.value = Double.NEGATIVE_INFINITY;
            //this.value = 0;
        }
        else {
            this.value = structure
                    .stream()
                    .mapToDouble(Coalition::getCoalitionValue)
                    .sum();
        }

    }

    public static int compareByTheValue(CoalitionStructure first, CoalitionStructure second) {
        return Double.compare(first.getValue(), second.getValue());

    }

}
