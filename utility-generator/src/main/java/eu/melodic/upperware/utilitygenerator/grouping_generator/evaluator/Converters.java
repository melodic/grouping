package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionElement;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTOFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
public class Converters {

    public static VariableDTO findGroupIdVariableForComponent(Collection<GroupingVariableDTO> groupIdVariables, String componentName) {
        return groupIdVariables.stream()
                .filter(groupId -> groupId.getComponentId().equals(componentName))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("GroupId for component: " + componentName + " was not found"));
    }

    public static Collection<PartitionElement> convertCoalitionStructure(Collection<GroupingVariableDTO> groupIdVariables, Collection<Coalition> coalitionStructure){
        //for each coalition, for all elements find a groupIdVariable and assign a number according to the counter
        int idOfCoalition = 0;
        Collection<VariableValueDTO> partitions = new ArrayList<>();
        Collection<PartitionElement> partition = new ArrayList<>();

        for (Coalition coalition : coalitionStructure) {

            for (CoalitionElement element : coalition.getElements()) {
                VariableDTO groupIdVariableForComponent = findGroupIdVariableForComponent(groupIdVariables, element.getComponentName());
                VariableValueDTO variableValueForComponent = VariableValueDTOFactory
                        .createElement(groupIdVariableForComponent.getId(), idOfCoalition, groupIdVariableForComponent);
                partitions.add(variableValueForComponent);
                partition.add(new PartitionElement(groupIdVariableForComponent.getId(), groupIdVariableForComponent.getComponentId(),
                        idOfCoalition, groupIdVariableForComponent));
            }
            idOfCoalition++;
        }
        return partition;
    }

    public static Collection<VariableValueDTO> convertPartitionElement(Collection<PartitionElement> partitionElements){
        return partitionElements.stream()
                .map(EvaluatingUtils::createElement)
                .collect(Collectors.toList());
    }

}
