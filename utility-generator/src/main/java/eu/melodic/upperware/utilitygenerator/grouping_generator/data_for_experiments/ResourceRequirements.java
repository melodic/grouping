package eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ResourceRequirements {
    private String componentName;
    private int cores;
    private int ram;
}
