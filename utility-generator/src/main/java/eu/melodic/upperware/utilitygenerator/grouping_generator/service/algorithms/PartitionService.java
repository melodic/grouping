package eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;

import java.util.Collection;

public interface PartitionService {


    GroupingAssignment findBestPartition(Collection<VariableValueDTO> solution, PartitionEvaluatorService partitionEvaluatorService);

    GroupingAssignment findBestPartition(ResourceRequirementsTable resourceRequirementsTable, PartitionEvaluatorService partitionEvaluatorService);
}
