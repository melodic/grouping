/* * Copyright (C) 2017 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.upperware.utilitygenerator.evaluator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.IntVariableValueDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
public class EvaluatingUtils {


    public static VariableValueDTO createElement(PartitionElement partitionElement){
        return new IntVariableValueDTO(partitionElement.getGroupIdVariableName(), partitionElement.getGroupNumber(), partitionElement.getVariableDTO());
    }

    public static boolean areUnmoveableComponentsMoved(Collection<String> unmoveableComponents, Collection<ConfigurationElement> actConfiguration, Collection<ConfigurationElement> newConfiguration) {
        return !newConfiguration.stream()
                .filter(component -> unmoveableComponents.contains(component.getId()))
                .allMatch(component -> actConfiguration.stream()
                        .anyMatch(actComponent -> actComponent.equals(component)));
    }

    public static Map<String, Integer> getCardinalitiesForComponent(Collection<VariableValueDTO> newConfiguration, Collection<VariableDTO> variables) {
        Map<String, Integer> cardinalitiesForComponent = new HashMap<>();

        Collection<VariableDTO> cardinalities = variables.stream()
                .filter(v -> VariableType.CARDINALITY.equals(v.getType()))
                .collect(Collectors.toList());

        newConfiguration.forEach(intVar -> cardinalities
                .stream()
                .filter(c -> intVar.getName().equals(c.getId()))
                .findFirst()
                .ifPresent(variable -> cardinalitiesForComponent.put(variable.getComponentId(), (int) intVar.getValue()))); //cardinality is always int
        return cardinalitiesForComponent;
    }

    //provider value is always int
    public static int getProviderValue(String componentId, Collection<VariableDTO> variables, Collection<VariableValueDTO> newConfigurationInt) {
        String provider = getVariableName(componentId, VariableType.PROVIDER, variables);
        return (int) newConfigurationInt.stream()
                .filter(intVar -> provider.equals(intVar.getName()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(format("Variable %s does not exist", provider)))
                .getValue();
    }

    public static VariableType getVariableType(String name, Collection<VariableDTO> variables) {
        return variables.stream()
                .filter(variable -> name.equals(variable.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(format("Variable %s does not exist", name)))
                .getType();
    }

    public static Collection<String> getVariablesForComponent(String componentId, Collection<VariableDTO> variables) {
        return variables.stream()
                .filter(variable -> componentId.equals(variable.getComponentId()))
                .map(VariableDTO::getId)
                .collect(Collectors.toList());
    }

    public static String getVariableName(String componentId, VariableType type, Collection<VariableDTO> variables) {
        return variables.stream()
                .filter(v -> ((componentId.equals(v.getComponentId())) && type.equals(v.getType())))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(format("Variable with type %s for component %s does not exist", type, componentId)))
                .getId();
    }

    public static Collection<String> getVariableForComponents(Collection<String> componentNames, Collection<VariableDTO> variables) {

        return variables.stream()
                .filter(variable -> componentNames.contains(variable.getComponentId()))
                .map(VariableDTO::getId)
                .collect(Collectors.toList());
    }

    public static List<VariableValueDTO> getVariableValues(Collection<VariableValueDTO> solution, Collection<VariableDTO> variables, VariableType variableType) {
        return CollectionUtils.emptyIfNull(solution).stream()
                .filter(variableValue -> variableType.equals(getVariableType(variableValue.getName(), variables)))
                .collect(Collectors.toList());
    }

    public static int getMustHaveVariableValueForComponent(Collection<VariableDTO> variables, Collection<VariableValueDTO> solution, String componentId, VariableType type) {
        Optional<VariableDTO> optionalVariableDTO = getVariableDTO(variables, componentId, type);
        String variableName = optionalVariableDTO
                .orElseThrow(() -> new IllegalStateException(format("There is an error, %s variable for component %s does not exist", type, componentId)))
                .getId();
        int value = getValueForVariable(solution, variableName);
        log.info("Found variable value: {} of type: {} , for component {}, ", value, type, componentId);
        return value;
    }

    public static Optional<Integer> getValueForComponent(Collection<VariableDTO> variables, Collection<VariableValueDTO> solution, String componentId, VariableType type) {
        Optional<VariableDTO> optionalVariableDTO = getVariableDTO(variables, componentId, type);
        if (optionalVariableDTO.isPresent()) {
            String variableName = optionalVariableDTO.get().getId();
            int value = getValueForVariable(solution, variableName);
            log.info("Found variable value: {} of type: {} , for component {}, ", value, type, componentId);
            return Optional.of(value);
        } else {
            return Optional.empty();
        }

    }

    public static Collection<String> getComponentNames(Collection<VariableDTO> variables){

        //uses the fact that the cardinality variable is defined for all components
        return variables.stream()
                .filter(variable -> VariableType.CARDINALITY.equals(variable.getType()))
                .map(VariableDTO::getComponentId)
                .collect(Collectors.toList());

    }

    private static Optional<VariableDTO> getVariableDTO(Collection<VariableDTO> variables, String componentId, VariableType type){
        return variables.stream()
                .filter(variable -> (componentId.equals(variable.getComponentId()) && type.equals(variable.getType())))
                .findAny();
    }

    private static Integer getValueForVariable(Collection<VariableValueDTO> solution, String variableName){
        return solution.stream()
                .filter(variableValue -> variableName.equals(variableValue.getName()))
                .findAny()
                .orElseThrow(() -> new IllegalStateException(format("There is an error, variable %s does not exist in the solution", variableName)))
                .getValue().intValue();
    }





}
