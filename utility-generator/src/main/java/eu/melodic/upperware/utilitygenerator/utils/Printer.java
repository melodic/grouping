/* * Copyright (C) 2018 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.upperware.utilitygenerator.utils;

import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;

@Slf4j
@AllArgsConstructor
@Getter
public class Printer {

    private Collection<VariableDTO> variables;

    public static void printSolution(Collection<VariableDTO> variables, Collection<VariableValueDTO> solution) {
        log.debug("Solution:");
        solution.stream()
                .filter(var -> variables.stream().anyMatch(v -> v.getId().equals(var.getName())))
                .forEach(filteredVar -> log.debug("{} = {} ", filteredVar.getName(), filteredVar.getValue()));
    }

}
