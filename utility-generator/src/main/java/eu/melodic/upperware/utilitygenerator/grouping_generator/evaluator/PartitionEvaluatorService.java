package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;

import java.util.Collection;

public interface PartitionEvaluatorService {

    //double evaluatePartition(Collection<VariableValueDTO> solution, Collection<PartitionElement> partition);
    double evaluatePartition(ResourceRequirementsTable requirementsTable, Collection<PartitionElement> partition);

    //double evaluateCoalition(Collection<VariableValueDTO> solution, Coalition coalition);
    double evaluateCoalition(ResourceRequirementsTable requirementsTable, Coalition coalition);
}
