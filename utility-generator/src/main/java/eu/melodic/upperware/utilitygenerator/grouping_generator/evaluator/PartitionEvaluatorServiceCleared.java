package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.NodeCandidatesTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.VirtualMachine;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.node_candidates.NodeCandidatesSelector;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.ClearedGroupingExperimentsApplication.GENERATING_DATA_FOR_ODPIP;


@AllArgsConstructor
public class PartitionEvaluatorServiceCleared implements PartitionEvaluatorService{

    private NodeCandidatesTable nodeCandidatesTable;
    private ConstraintChecker constraintChecker;

    //@Override
    public double evaluatePartition(Collection<VariableValueDTO> solution, Collection<PartitionElement> partition) {
        return 0;
    }

    @Override
    public double evaluatePartition(ResourceRequirementsTable requirementsTable, Collection<PartitionElement> partition) {

        //log.debug("Evaluating partition: {}", partition.toString());
        boolean validPartition = constraintChecker.checkConstraintsSatisfaction(partition);
        //log.debug("Result of validating parition: {}", validPartition);

        if (validPartition) {
        }
        return 0.0;

    }

    //@Override
    public double evaluateCoalition(Collection<VariableValueDTO> solution, Coalition coalition) {
        return 0;
    }

    @Override
    public double evaluateCoalition(ResourceRequirementsTable requirementsTable, Coalition coalition) {

        //log.debug("Evaluating coalition: {}", coalition.toString());

        //if not generating, then these coalitions are not present
        if (GENERATING_DATA_FOR_ODPIP){
            if (!constraintChecker.checkGroupedWithConstraints(coalition.getElements())){
                //System.out.println("not valid coalition, returning -infinity");
                //log.info("not valid coalition, returning -infinity");
                return Double.NEGATIVE_INFINITY;
            }
        }

        Optional<VirtualMachine> cheapestVMopt = nodeCandidatesTable.findCheapestVM(requirementsTable, coalition);
        if (cheapestVMopt.isPresent()){
            //double costUtilityNormalized = - cheapestVMopt.get().getPrice() / (nodeCandidatesTable.getMAX_PRICE() - nodeCandidatesTable.getMIN_PRICE());
            double costValueFunction = coalition.getComponentNames().size() * nodeCandidatesTable.getMAX_PRICE() - cheapestVMopt.get().getPrice();
            return costValueFunction;
        }
        else{
            //log.warn("No Node Candidate for this group");
            return Double.NEGATIVE_INFINITY;
        }

    }
}
