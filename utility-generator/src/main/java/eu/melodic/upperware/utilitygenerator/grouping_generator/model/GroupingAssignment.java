package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.Converters.convertPartitionElement;


@Getter
@ToString
public class GroupingAssignment {

    private double utilityValue;
    private Collection<VariableValueDTO> partition;
    @Setter
    private int counterCSG;

    public GroupingAssignment(GroupingAssignment ga){
        this.counterCSG = ga.counterCSG;
        this.partition = ga.partition;
        this.utilityValue = ga.utilityValue;
    }
    public GroupingAssignment(double utilityValue, Collection<PartitionElement> partition){
        this.utilityValue = utilityValue;
        this.partition = convertPartitionElement(partition);
    }

    public GroupingAssignment(double utilityValue, Collection<PartitionElement> partition, int counterCSG){
        this.utilityValue = utilityValue;
        this.partition = convertPartitionElement(partition);
        this.counterCSG = counterCSG;
    }

}
