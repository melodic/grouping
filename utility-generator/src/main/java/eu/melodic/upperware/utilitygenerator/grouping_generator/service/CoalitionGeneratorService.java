package eu.melodic.upperware.utilitygenerator.grouping_generator.service;

import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.ConstraintChecker;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.AllCoalitionsStructure;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionList;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.ClearedGroupingExperimentsApplication.GENERATING_DATA_FOR_ODPIP;

@Slf4j
public class CoalitionGeneratorService {

    private ConstraintChecker constraintChecker;


    public AllCoalitionsStructure generateAllCoalitions(Collection<String> components, GrouperData data, boolean isODPIP) {
        this.constraintChecker = new ConstraintChecker(data);

        Map<Integer, CoalitionList> allCoalitions = new HashMap<>();
        for (int size = 1; size <= components.size(); size++) {
            List<Coalition> coalitionsWithSize = generateCoalitions(components, size);

            if (!isODPIP){
                //log.debug("Number of coalitions before prunning: {}", coalitionsWithSize.size());
                coalitionsWithSize = pruneNotValidCoalitions(coalitionsWithSize);
                //log.debug("Number of coalitions after prunning: {}", coalitionsWithSize.size());

            }

            CoalitionList coalitionList = new CoalitionList(coalitionsWithSize);
            allCoalitions.put(size, coalitionList);
        }
        //log.debug("Generated coalitions for components: {}\n {}", components, allCoalitions);
        //log.info("Number of generated coalitions: {}", allCoalitions.values().stream().mapToInt(coalition -> coalition.getCoalitions().size()).sum());

        return new AllCoalitionsStructure(allCoalitions);
    }

    private List<Coalition> generateCoalitions(Collection<String> components, int size) {
        List<String> componentsSorted = components.stream().sorted().collect(Collectors.toList());


        List<Coalition> result = new ArrayList<>();
        if (size < 1) {
            return Collections.emptyList();
        }
        for (int index = 0; index < componentsSorted.size(); index++) {
            String componentId = componentsSorted.get(index);
            CoalitionElement component = new CoalitionElement(componentId);
            //log.debug("For component: {}", componentId);
            if (size == 1) {
                //log.debug("Size == 1");
                result.add(new Coalition(new HashSet<>(Collections.singletonList(component))));
            } else {
                List<String> componentWithoutActual = componentsSorted.subList(index + 1, componentsSorted.size());
                //log.debug("Components without actual: {}", componentWithoutActual);
                List<Coalition> smallerCoalitions = generateCoalitions(componentWithoutActual, size - 1);
                //log.debug("Smaller coalitions size: {}", smallerCoalitions.size());
                for (Coalition smallerCoalition : smallerCoalitions) {
                    Set<CoalitionElement> newCoalitionElements = smallerCoalition.getElements();
                    newCoalitionElements.add(new CoalitionElement(componentId));
                    result.add(new Coalition(newCoalitionElements));
                }
            }
        }
        //log.debug("Generated coalitions for size: {} : {}", size, result.toString());
        return result;
    }


    private List<Coalition> pruneNotValidCoalitions(List<Coalition> coalitions) {
        return coalitions.stream()
                .filter(coalition -> constraintChecker.checkGroupedWithConstraints(coalition.getElements()))
                .collect(Collectors.toList());
    }


}
