package eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class AllComponentsTogetherAlgorithm implements PartitionService {

    private GrouperData data;

    //@Override
    public GroupingAssignment findBestPartition(Collection<VariableValueDTO> solution, PartitionEvaluatorService partitionEvaluatorService) {

        log.info("Using AllComponentsTogether Algorithm");
        List<PartitionElement> partition = data.getGroupIdVariables().stream()
                .map(groupIdVariable ->
                        new PartitionElement(
                                groupIdVariable.getId(),
                                groupIdVariable.getComponentId(),
                                0,
                                groupIdVariable))
                .collect(Collectors.toList());

        //double utilityValue = partitionEvaluatorService.evaluatePartition(solution, partition);
        double utilityValue = 1.0;
        if (utilityValue == 0) {
            log.warn("Valid partition for this solution does not exist, returning assignment with utility value 0.0");
        }
        return new GroupingAssignment(utilityValue, partition);
    }

    @Override
    public GroupingAssignment findBestPartition(ResourceRequirementsTable resourceRequirementsTable, PartitionEvaluatorService partitionEvaluatorService) {
        return null;
    }
}
