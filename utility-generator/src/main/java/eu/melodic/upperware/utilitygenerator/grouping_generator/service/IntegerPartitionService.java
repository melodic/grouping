package eu.melodic.upperware.utilitygenerator.grouping_generator.service;

import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartitionSet;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class IntegerPartitionService {


    public IntegerPartitionSet generateAllIntegerPartitions(int number) {
        IntegerPartitionSet result = new IntegerPartitionSet(number);
        partition(number, number, new IntegerPartition(), result);
        //log.info("Generated interer partitions: {}", result.getPartitions().size());
        return result;
    }

    private void partition(int n, int max, IntegerPartition currentPartition, IntegerPartitionSet allPartitions) {
        if (n == 0) {
            //log.debug("Created partition: {}", currentPartition);
            allPartitions.getPartitions().add(currentPartition);
            return;
        }
        for (int i = Math.min(max, n); i >= 1; i--) {
            List<Integer> newPartition = new ArrayList<>(currentPartition.getPattern());
            newPartition.add(i);
            partition(n - i, i, new IntegerPartition(newPartition), allPartitions);
        }
    }


    public IntegerPartitionSet filterPartitionsThatContainKParts(IntegerPartitionSet setToFilter, int k){
        List<IntegerPartition> filteredPartitions = setToFilter.getPartitions().stream()
                .filter(partition -> partition.getPattern().size() == k)
                .collect(Collectors.toList());
        return new IntegerPartitionSet(filteredPartitions, setToFilter.getNumber());

    }
}
