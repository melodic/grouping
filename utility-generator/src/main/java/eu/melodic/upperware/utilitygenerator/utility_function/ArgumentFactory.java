/* * Copyright (C) 2018 7bulls.com
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.upperware.utilitygenerator.utility_function;

import eu.passage.upperware.commons.model.tools.cpmodel.DTO.*;
import org.mariuszgromada.math.mxparser.Argument;

public class ArgumentFactory {

    public static Argument createArgument(VariableValueDTO variableValueDTO) {
        if (variableValueDTO instanceof IntVariableValueDTO) {
            return new Argument(variableValueDTO.getName(), ((IntVariableValueDTO) variableValueDTO).getValue());
        } else { //RealVariableValueDTO
            return new Argument(variableValueDTO.getName(), ((RealVariableValueDTO) variableValueDTO).getValue());
        }
    }

    public static Argument createArgument(String name, Number value) {
        if (value instanceof Integer) {
            return new Argument(name, (int) value);
        } else { //Double
            return new Argument(name, (double) value);
        }
    }


    public static Argument createArgument(MetricDTO metric) {
        if (metric instanceof IntMetricDTO) {
            return new Argument(metric.getName(), ((IntMetricDTO) metric).getValue());
        } else if (metric instanceof DoubleMetricDTO) {
            return new Argument(metric.getName(), ((DoubleMetricDTO) metric).getValue());
        } else { //Float
            return new Argument(metric.getName(), (double) ((FloatMetricDTO) metric).getValue());
        }
    }
}
