package eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class AllCoalitionsStructure {

    //map size of coalitions, list of coalitions with this size
    private Map<Integer, CoalitionList> coalitions;

}
