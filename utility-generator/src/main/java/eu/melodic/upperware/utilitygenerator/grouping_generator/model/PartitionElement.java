package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class PartitionElement {

    private String groupIdVariableName;
    private String componentName;
    private Integer groupNumber;
    private VariableDTO variableDTO;

}
