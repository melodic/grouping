package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import eu.melodic.cache.NodeCandidates;
import eu.paasage.upperware.metamodel.cp.CpVariable;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashSet;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@ToString
@Slf4j
public class NewGrouperData {

    private Collection<VariableDTO> basicVariables = new HashSet<>();
    @Setter
    private NodeCandidates nodeCandidates;
    private Collection<GroupingVariableDTO> groupIdVariables = new HashSet<>();
    private Collection<GroupingVariableDTO> groupedWithVariables = new HashSet<>();
    private Collection<Pair<GroupingVariableDTO, GroupingVariableDTO>> mustBeTogetherCoupling = new HashSet<>();

    public void addCpVariable(CpVariable cpVariable) {
        switch (cpVariable.getVariableType()) {
            case GROUP_ID:
                log.info("Adding GroupedWith Variable to GrouperData: {}", cpVariable.getId());
                this.groupIdVariables.add(new GroupingVariableDTO(cpVariable));
                break;
            case GROUPED_WITH:
                log.info("Adding GroupedWith Variable to GrouperData: {}", cpVariable.getId());
                this.groupedWithVariables.add(new GroupingVariableDTO(cpVariable));
                break;
            default:
                log.info("Adding basic Variable to GrouperData: {}", cpVariable.getId());
                this.basicVariables.add(new VariableDTO(cpVariable));
        }
    }

    public void addCoupling(CpVariable cpVariable1, CpVariable cpVariable2) {
        log.info("Adding must bo together coupling to GrouperData: {} & {}", cpVariable1.getId(), cpVariable2.getComponentId());
        this.mustBeTogetherCoupling.add(new ImmutablePair<>(new GroupingVariableDTO(cpVariable1), new GroupingVariableDTO(cpVariable2)));
    }

    public Collection<VariableDTO> getAllVariables() {
        Collection<VariableDTO> allVariables = new HashSet<>();
        allVariables.addAll(basicVariables);
        allVariables.addAll(groupedWithVariables);
        allVariables.addAll(groupIdVariables);
        return allVariables;
    }

}
