package eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class Coalition {

    private Set<CoalitionElement> elements;
    @Setter
    private double coalitionValue;

    public Coalition(Set<CoalitionElement> elements) {
        this.elements = elements;
    }



    @Override
    public String toString() {

        return "value: " + coalitionValue
                + ", elements: " + elements.stream().map(CoalitionElement::getComponentName).collect(Collectors.toList()).toString();
    }

    public Collection<String> getComponentNames(){
        return this.getElements()
                .stream()
                .map(CoalitionElement::getComponentName)
                .collect(Collectors.toList());

    }

}
