package eu.melodic.upperware.utilitygenerator.grouping_generator;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import eu.melodic.cache.CacheService;
import eu.melodic.cache.NodeCandidates;
import eu.melodic.cache.impl.FilecacheService;
import eu.melodic.upperware.penaltycalculator.PenaltyFunctionProperties;
import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.NewGrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms.RahwanAnyTimeAlgorithm;
import eu.melodic.upperware.utilitygenerator.properties.UtilityGeneratorProperties;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.paasage.upperware.security.authapi.properties.MelodicSecurityProperties;
import eu.paasage.upperware.security.authapi.token.JWTService;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.IntVariableValueDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//@Getter
/*public class GroupingExperimentsApplication {

    @Setter
    private GrouperData grouperData;
    private NodeCandidates samplerNodeCandidates;
    private NodeCandidates limitedNodeCandidates;
    private CacheService<NodeCandidates> filecacheService = new FilecacheService();

    private UtilityGeneratorProperties ugproperties = new UtilityGeneratorProperties();
    private MelodicSecurityProperties securityProperties = new MelodicSecurityProperties();
    private PenaltyFunctionProperties properties = new PenaltyFunctionProperties();
    private JWTService jwtService;

    public static final boolean GENERATING_DATA_FOR_ODPIP = false;
    public static final boolean ONLY_ONE_LOOP=true;
    public static final boolean SAVE_COUNTERS=false;
    public static final boolean CONSTRAINTS_ENABLED=false;
    public static final String FILE_INFIX="v13-20221121";

    public static final String NC_PATH = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/grouping/FCRclear1585925784069";
    public static final String CAMEL_MODEL_PATH = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/grouping/FCRnewGrouping6.xmi";
    public static final String CP_MODEL_PATH = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/grouping/FCRclear1585925784069.xmi";

    public static final String CONFIGURATIONS_PATH_6="/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/Configurations-6-smaller.csv";
    public static final String CONFIGURATIONS_PATH_5="/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/5components-Configurations.csv";
    public static final String CONFIGURATIONS_PATH_7 = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/7components-Configurations.csv";
    public static final String CONFIGURATIONS_PATH_8 = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/8components-Configurations.csv";
    public static final String CONFIGURATIONS_PATH_3 = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/3components-Configurations.csv";
    public static final String CONFIGURATIONS_PATH = "/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/4components-Configurations.csv";


    public static final String INIT_COALITION_VALUES_PATH = "/Users/mrozanska/Desktop/grouping-paper-data/4components/";
    public static String COALITION_VALUES_PATH = INIT_COALITION_VALUES_PATH;
    public static final String CONSTRAINTS_DEFINITIONS_FOLDER="/Users/mrozanska/git/upperware/utility-generator/src/main/test/resources/experiments/constraints/";


    public static void main(String[] args){

        GroupingExperimentsApplication experiments = new GroupingExperimentsApplication();
        experiments.setup(NC_PATH);

        Collection<String> components = Arrays.asList("A", "B", "C", "D");

        Collection<VariableDTO> variables = new ArrayList<>(Arrays.asList(generateVariablesDTO(components)));

        Collection<Collection<VariableValueDTO>> allSolutions = convertCsvToSolutionList(CONFIGURATIONS_PATH, components);

        for (String componentName: components){
            variables.add(new VariableDTO("provider_" + componentName,componentName, VariableType.PROVIDER));
        }


        Collection<Collection<GroupingVariableDTO>> constraintsCases = new ArrayList<>();

        if (CONSTRAINTS_ENABLED){
            String[] pathnames;

            // Creates a new File instance by converting the given pathname string
            // into an abstract pathname
            File f = new File(CONSTRAINTS_DEFINITIONS_FOLDER);

            // Populates the array with names of files and directories
            pathnames = f.list();

            for (String pathname : pathnames){
                Collection<GroupingVariableDTO> groupedWithVariables;
                groupedWithVariables = convertGroupedWithFromCsv(CONSTRAINTS_DEFINITIONS_FOLDER+"/" + pathname, components);
                constraintsCases.add(groupedWithVariables);
                if (GENERATING_DATA_FOR_ODPIP){
                    COALITION_VALUES_PATH = COALITION_VALUES_PATH + pathname + "/";

                    if (new File(COALITION_VALUES_PATH).exists()){
                        System.out.println("This folder exists, skipping: " + COALITION_VALUES_PATH);
                        COALITION_VALUES_PATH = INIT_COALITION_VALUES_PATH;
                        continue;
                    }
                    else {
                        new File(COALITION_VALUES_PATH).mkdirs();
                    }
                }
                runExperiment(pathname, experiments, components, variables, allSolutions, groupedWithVariables);
                COALITION_VALUES_PATH = INIT_COALITION_VALUES_PATH;
                //constraintsCases.forEach(constraints -> runExperiment(pathname, experiments, components, variables, allSolutions, constraints));

            }

        }
        else{

            runExperiment("no-constraints", experiments, components, variables, allSolutions, generateGroupedWithVariables(components));

        }


    }

    private static void runExperiment(String name, GroupingExperimentsApplication experiments, Collection<String> components, Collection<VariableDTO> variables, Collection<Collection<VariableValueDTO>> allSolutions, Collection<GroupingVariableDTO> groupedWithVariables){

        Collection<GroupingVariableDTO> groupIdVariables = generateGroupIdVariables(components);

        NewGrouperData grouperData = new NewGrouperData(variables, experiments.getSamplerNodeCandidates(), groupIdVariables, groupedWithVariables, null);
        UtilityGeneratorApplication utilityGenerator = new UtilityGeneratorApplication(CAMEL_MODEL_PATH, CP_MODEL_PATH, true, experiments.getSamplerNodeCandidates(), experiments.getUgproperties(),experiments.getSecurityProperties(), experiments.getJwtService(), experiments.getProperties());
        GroupingGeneratorApplication groupingGeneratorApplication = new GroupingGeneratorApplication(utilityGenerator, grouperData, new RahwanAnyTimeAlgorithm(grouperData));

        Collection<Integer> checkedCsg = new ArrayList<>();

        int i = 0;
        for (Collection<VariableValueDTO> solution: allSolutions){

            if (ONLY_ONE_LOOP && (i>1)) {
                break;
            }

            GroupingAssignment result = groupingGeneratorApplication.findTheBestPartition(solution);
            checkedCsg.add((int) result.getUtilityValue());
            if (ONLY_ONE_LOOP){
                System.out.println("checked: "+ checkedCsg);
            }


            i++;
        }


        System.out.println("number of checked csg" + checkedCsg.size());


//        final ChartsGenerator generator = new ChartsGenerator("Box-and-Whisker Chart Demo", checkedCsg);
//        generator.pack();
//        RefineryUtilities.centerFrameOnScreen(generator);
//        generator.setVisible(true);

        if (SAVE_COUNTERS) {
            try {
                givenDataArray_whenConvertToCSV_thenOutputCreated(components.size()+ "csg_counters_" + "sol_" + allSolutions.size() + FILE_INFIX+name, checkedCsg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    private static Collection<GroupingVariableDTO> convertGroupedWithFromCsv(String path, Collection<String> components){

        Collection<GroupingVariableDTO> groupedWithVariables= new ArrayList<>();
        //assumption: data is starting on line 17
        CSVReader reader;
        int counter = 0;
        try {
            reader = new CSVReader(new FileReader(path));
            String[] line;
            while ((line = reader.readNext()) != null) {
                if (counter > 15){

                    List<Integer> domain = new ArrayList<>();
                    for (int i=1;i< line.length;  i++) {
                        if (!line[i].isEmpty()){
                            domain.add(Integer.valueOf(line[i]));
                        }
                    }
                    GroupingVariableDTO groupingVariableDTO= new GroupingVariableDTO(line[0], line[0], VariableType.GROUPED_WITH, domain);
                    System.out.println("GroupedWith: " + groupingVariableDTO);

                    groupedWithVariables.add(groupingVariableDTO);
                }
                counter++;
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
        //System.out.println("Size of groupedWith variables: {}"+ groupedWithVariables.size());
        return groupedWithVariables;
    }

    private static Collection<Collection<VariableValueDTO>> convertCsvToSolutionList(String path, Collection<String> components){
        VariableDTO[] variables = generateVariablesDTO(components);
        Collection<Collection<VariableValueDTO>> solutions= new ArrayList<>();

        int limit = 1000000;
        int counter = 0;

        CSVReader reader;
        try {
            reader = new CSVReader(new FileReader(path));
            String[] line;
            while ((line = reader.readNext()) != null) {
                if (counter > limit){
                    break;
                }
                Collection<VariableValueDTO> solution = new ArrayList<>();
                for (int i=0;i< line.length;  i++) {
                    solution.add(new IntVariableValueDTO(variables[i].getId(), Integer.valueOf(line[i]), variables[i]));

                }
                //System.out.println("Solution= " + solution);
                for (String componentName: components){
                    solution.add(new IntVariableValueDTO("provider_" + componentName, 0, new VariableDTO("provider_" + componentName,componentName, VariableType.PROVIDER)));
                }
                solutions.add(solution);
                counter++;

            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
        System.out.println("Size of solution space: {}"+ solutions.size());
        return solutions;
    }



    private static VariableDTO[] generateVariablesDTO(Collection<String> components){
        String cardinalityPrefix = "cardinality_";
        String providerPrefix = "provider_";

        String coresPrefix = "Cores";
        String ramPrefix = "Memory";


        VariableDTO[] variables = new VariableDTO[components.size()*3];

        int i = 0;
        for (String componentName : components) {
            variables[i] = new VariableDTO(cardinalityPrefix + componentName, componentName, VariableType.CARDINALITY);
            i++;
            variables[i] = new VariableDTO(coresPrefix + componentName, componentName, VariableType.CORES);
            i++;
            variables[i] = new VariableDTO(ramPrefix + componentName, componentName, VariableType.RAM);
            i++;

            //variables.add(new VariableDTO(cardinalityPrefix + componentName, componentName, VariableType.CARDINALITY));
            //variables.add(new VariableDTO(coresPrefix + componentName, componentName, VariableType.CORES));
            //variables.add(new VariableDTO(ramPrefix + componentName, componentName, VariableType.RAM));
        }
        return variables;
    }


    private void setup(String pathNodeCandidates){
        this.filecacheService = new FilecacheService();

        this.samplerNodeCandidates = this.filecacheService.load(pathNodeCandidates);

        Map<Integer, List<NodeCandidate>> candidatesForGrouper = new HashMap<>();

        for (Integer provider : this.samplerNodeCandidates.getCandidatesForGrouper().keySet()) {
            List<NodeCandidate> listNodeCandidates = this.samplerNodeCandidates.getCandidatesForGrouper().get(provider);
            List<NodeCandidate> newList = new ArrayList<>();
            for (NodeCandidate nc : listNodeCandidates) {
                if (nc.getPrice() < 1000) {
                    newList.add(nc);
                }
            }
            candidatesForGrouper.put(provider, newList);
        }
        limitedNodeCandidates = new NodeCandidates(samplerNodeCandidates.get(), candidatesForGrouper, Collections.emptyMap());
        limitedNodeCandidates.getCandidatesForGrouper();


        ugproperties.setUtilityGenerator(new UtilityGeneratorProperties.UtilityGenerator());
        ugproperties.getUtilityGenerator().setDlmsControllerUrl("");


    }

    private static Collection<GroupingVariableDTO> generateGroupIdVariables(Collection<String> components){
        Collection<GroupingVariableDTO> groupIdVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();
        String groupIdPrefix = "group_Id_";

        int counter = 0;
        for (String componentName : components) {
            groupIndicies.add(counter);
            groupIdVariables.add(new GroupingVariableDTO(groupIdPrefix + componentName, componentName, VariableType.GROUP_ID, groupIndicies));
        counter++;
        }
        return groupIdVariables;
    }

    private static Collection<GroupingVariableDTO> generateGroupedWithVariables(Collection<String> components){
        Collection<GroupingVariableDTO> groupedWithVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();

        String groupedWithPrefix = "groupedWith_";

        int counter = 0;
        for (String componentName : components) {
            groupIndicies.add(counter);
            groupedWithVariables.add(new GroupingVariableDTO(groupedWithPrefix + componentName, componentName, VariableType.GROUPED_WITH, groupIndicies));
            //groupedWithVariables.add(new GroupingVariableDTO(groupedWithPrefix + componentName, componentName, VariableType.GROUPED_WITH, Collections.singletonList(counter)));
            counter++;
        }
        return groupedWithVariables;
    }

    private static void givenDataArray_whenConvertToCSV_thenOutputCreated(String fileName, Collection<Integer> values) throws IOException {
        File csvOutputFile = new File(fileName);
        List<String[]> dataLines = new ArrayList<>();

//        String[] firstLine = new String[values.size()];
//
//        int counter = 0;
//        for (Integer value: values){
//            firstLine[counter] = Integer.toString(value);
//            counter++;
//        }
//        dataLines.add(firstLine);
        values.forEach(value -> {
            String[] array = new String[1];
            array[0] = Integer.toString(value);
            dataLines.add(array);
        });
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(GroupingExperimentsApplication::convertToCSV)
                    .forEach(pw::println);
        }
    }

    private static String convertToCSV(String[] data) {
        return Stream.of(data)
                .collect(Collectors.joining(","));
    }



    private static Collection<GroupingVariableDTO> generateGroupedWithVariablesConstraints(Collection<String> components){
        Collection<GroupingVariableDTO> groupedWithVariables = new ArrayList<>();
        List<Integer> groupIndicies = new LinkedList<>();

        String groupedWithPrefix = "groupedWith_";


        /*
        v1: Component Name	Can be grouped with
        A	A	B	C	D
        B	A	B	C	D
        C	A	B	C	D
        D	A	B	C	D
        E	E


        */ /*
        GroupingVariableDTO groupingVariableDTO_A = new GroupingVariableDTO(groupedWithPrefix + "A", "A",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_B = new GroupingVariableDTO(groupedWithPrefix + "B", "B",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_C = new GroupingVariableDTO(groupedWithPrefix + "C", "C",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0,1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_D = new GroupingVariableDTO(groupedWithPrefix + "D", "D",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3)));
        GroupingVariableDTO groupingVariableDTO_E = new GroupingVariableDTO(groupedWithPrefix + "E", "E",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(4)));

        groupedWithVariables.add(groupingVariableDTO_A);
        groupedWithVariables.add(groupingVariableDTO_B);
        groupedWithVariables.add(groupingVariableDTO_C);
        groupedWithVariables.add(groupingVariableDTO_D);
        groupedWithVariables.add(groupingVariableDTO_E);

*/
        /*
        v2:
        Component Name	Can be grouped with
        A	A	C	D	E
        B	B	C	D	E
        C	A	B	C	D	E
        D	A	B	C	D	E
        E	A	B	C	D	E

        * */

 /*       GroupingVariableDTO groupingVariableDTO_A = new GroupingVariableDTO(groupedWithPrefix + "A", "A",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 2 , 3, 4)));
        GroupingVariableDTO groupingVariableDTO_B = new GroupingVariableDTO(groupedWithPrefix + "B", "B",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(1, 2, 3, 4)));
        GroupingVariableDTO groupingVariableDTO_C = new GroupingVariableDTO(groupedWithPrefix + "C", "C",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3,4)));
        GroupingVariableDTO groupingVariableDTO_D = new GroupingVariableDTO(groupedWithPrefix + "D", "D",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1 , 2, 3,4)));
        GroupingVariableDTO groupingVariableDTO_E = new GroupingVariableDTO(groupedWithPrefix + "E", "E",
                VariableType.GROUPED_WITH, new ArrayList<>(Arrays. asList(0, 1,2,3,4)));

        groupedWithVariables.add(groupingVariableDTO_A);
        groupedWithVariables.add(groupingVariableDTO_B);
        groupedWithVariables.add(groupingVariableDTO_C);
        groupedWithVariables.add(groupingVariableDTO_D);
        groupedWithVariables.add(groupingVariableDTO_E);


        return groupedWithVariables;
    }
}
*/