package eu.melodic.upperware.utilitygenerator.grouping_generator.model;

import eu.melodic.cache.NodeCandidates;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.paasage.upperware.metamodel.cp.CpVariable;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashSet;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@ToString
@Slf4j
public class GrouperData {

    private ResourceRequirementsTable resourceRequirementsTable;
    private Collection<GroupingVariableDTO> groupIdVariables = new HashSet<>();
    private Collection<GroupingVariableDTO> groupedWithVariables = new HashSet<>();


}
