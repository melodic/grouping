package eu.melodic.upperware.utilitygenerator.grouping_generator;

import lombok.Getter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerXYDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.ui.ApplicationFrame;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ChartsGenerator extends ApplicationFrame {

    @Getter
    private JFreeChart chart;
    public ChartsGenerator(String titel, Collection<Integer> values) {
        super(titel);

//        final BoxAndWhiskerXYDataset dataset = createDataset(new ArrayList<>(values));
//        final JFreeChart chart = createChart(dataset);
//
//        final ChartPanel chartPanel = new ChartPanel(chart);
//        chartPanel.setPreferredSize(new java.awt.Dimension(500, 300));
//        setContentPane(chartPanel);





        final BoxAndWhiskerCategoryDataset dataset = createDataset(new ArrayList<>(values));

        final CategoryAxis xAxis = new CategoryAxis("Type");
        final NumberAxis yAxis = new NumberAxis("Value");
        yAxis.setAutoRangeIncludesZero(false);
        final BoxAndWhiskerRenderer renderer = new BoxAndWhiskerRenderer();
        renderer.setFillBox(false);
        renderer.setMedianVisible(true);
        final CategoryPlot plot = new CategoryPlot(dataset, xAxis, yAxis, renderer);

        final JFreeChart chart = new JFreeChart(
                "Box-and-Whisker Demo",
                new Font("SansSerif", Font.BOLD, 14),
                plot,
                true
        );
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
        setContentPane(chartPanel);

//        OutputStream out = null;
//        try {
//            out = new FileOutputStream("verspecificnamefile");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        //ChartUtilities.saveChartAsPNG(out, chart, 600,300);
//        try {
//            ChartUtilities.writeChartAsPNG(out, chart, 600, 300);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        this.chart = chart;

    }



    private JFreeChart createChart(
            final BoxAndWhiskerXYDataset dataset) {
        JFreeChart chart = ChartFactory.createBoxAndWhiskerChart(
                "Box and Whisker Chart", "Time", "Value", dataset, true);
        //chart.setBackgroundPaint(new Color(249, 231, 236));

        return chart;
    }
//
//    public static void main(final String[] args) {
//
//        final BoxAndWhiskerChart demo = new BoxAndWhiskerChart("");
//        demo.pack();
//        RefineryUtilities.centerFrameOnScreen(demo);
//        demo.setVisible(true);
//    }


    private BoxAndWhiskerCategoryDataset createDataset(List<Integer> values) {



        final int seriesCount = 1;
        final int categoryCount = 1;
        final int entityCount = 22;

        final DefaultBoxAndWhiskerCategoryDataset dataset
                = new DefaultBoxAndWhiskerCategoryDataset();
        for (int i = 0; i < seriesCount; i++) {
            for (int j = 0; j < categoryCount; j++) {
                dataset.add(values, "Series " + i, " Type " + j);

            }

        }

        return dataset;
    }
}