package eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@AllArgsConstructor
public class EachComponentSeparatelyAlgorithm implements PartitionService {

    private GrouperData data;


    //@Override
    public GroupingAssignment findBestPartition(Collection<VariableValueDTO> solution, PartitionEvaluatorService partitionEvaluatorService) {

        log.info("Using Each Component Separately Algorithm");

        List<PartitionElement> partition = new ArrayList<>();
        int counter = 0;
        for (VariableDTO groupIdVariable : data.getGroupIdVariables()) {
            partition.add(new PartitionElement(groupIdVariable.getId(), groupIdVariable.getComponentId(), counter, groupIdVariable));
            counter++;
        }
        log.info("Partition to evaluate: {}", partition.toString());

        double utilityValue = 1.0; //partitionEvaluatorService.evaluatePartition(solution, partition);

        return new GroupingAssignment(utilityValue, partition);
    }

    @Override
    public GroupingAssignment findBestPartition(ResourceRequirementsTable resourceRequirementsTable, PartitionEvaluatorService partitionEvaluatorService) {
        return null;
    }
}
