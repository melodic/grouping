package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirements;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionElement;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.GroupingVariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils.getMustHaveVariableValueForComponent;
import static eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils.getValueForComponent;
import static eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionToConfigurationElementConverter.groupComponents;

@Slf4j
public class ConstraintChecker {

    private GrouperData data;
    private Map<String, Integer> componentIds;


    public ConstraintChecker(GrouperData data) {
        this.data = data;
        this.componentIds = new HashMap<>();
        List<String> components = data.getGroupIdVariables().stream()
                .map(VariableDTO::getComponentId)
                .sorted()
                .collect(Collectors.toList());
        components.forEach(id -> this.componentIds.put(id, components.indexOf(id)));
        //log.info("Mapping between component Id and the id in grouped With variable: {}", componentIds.toString());

    }

    public boolean checkConstraintsSatisfaction(Collection<PartitionElement> partition) {

        //todo: check if components have to be together (constraints)
        Collection<Set<PartitionElement>> groups = groupComponents(partition);
        //log.info("Checking constraints satisfaction");
        return groups.stream()
                .allMatch(this::checkGroupedWithConstraints);
    }

    /*public boolean checkConstraintsSatisfaction(Collection<VariableValueDTO> solution, Collection<PartitionElement> partition) {

        //todo: check if components have to be together (constraints)
        Collection<Set<PartitionElement>> groups = groupComponents(partition);
        //log.info("Checking constraints satisfaction");
        return groups.stream()
                .allMatch(group -> checkIfPropertiesAreTheSame(solution, group)
                        && checkGroupedWithConstraints(group));
    }*/

    /*private boolean checkIfPropertiesAreTheSame(Collection<VariableValueDTO> solution, Collection<PartitionElement> partition) {
        Optional<PartitionElement> representativeOptional = partition.stream().findAny();
        if (representativeOptional.isPresent()) {
            PartitionElement representative = representativeOptional.get();
            int cardinalityRepresentative = getMustHaveVariableValueForComponent(data.getBasicVariables(), solution, representative.getComponentName(), VariableType.CARDINALITY);
            Optional<Integer> latitudeRepresentative = getValueForComponent(data.getBasicVariables(), solution, representative.getComponentName(), VariableType.LATITUDE);
            Optional<Integer> longitudeRepresentative = getValueForComponent(data.getBasicVariables(), solution, representative.getComponentName(), VariableType.LONGITUDE);
            Optional<Integer> operatingSystemRepresentative = getValueForComponent(data.getBasicVariables(), solution, representative.getComponentName(), VariableType.OS);
            boolean result = partition.stream()
                    .allMatch(partitionElement ->
                            ((Objects.equals(cardinalityRepresentative, getMustHaveVariableValueForComponent(data.getBasicVariables(), solution, partitionElement.getComponentName(), VariableType.CARDINALITY))))
                                    && (Objects.equals(latitudeRepresentative.orElse(0), getValueForComponent(data.getBasicVariables(), solution, partitionElement.getComponentName(), VariableType.LATITUDE).orElse(0)))
                                    && (Objects.equals(longitudeRepresentative.orElse(0), getValueForComponent(data.getBasicVariables(), solution, partitionElement.getComponentName(), VariableType.LONGITUDE).orElse(0)))
                                    && (Objects.equals(operatingSystemRepresentative.orElse(0), getValueForComponent(data.getBasicVariables(), solution, partitionElement.getComponentName(), VariableType.OS).orElse(0))
                            ));
            //log.debug("Checking constraint satisfaction for: {}, result: {}", representative.getComponentName(), result);
            return result;

        } else {
            return true;
        }

    }*/

    private boolean checkGroupedWithConstraints(Collection<PartitionElement> group) {
        List<Integer> componentIndices = group.stream()
                .map(element -> componentIds.get(element.getComponentName()))
                .collect(Collectors.toList());
        for (PartitionElement partitionElement : group) {
            String componentName = partitionElement.getComponentName();
            Optional<GroupingVariableDTO> variableForComponentOpt = data.getGroupedWithVariables().stream()
                    .filter(variable -> componentName.equals(variable.getComponentId()))
                    .findAny();
            if (variableForComponentOpt.isPresent()) {
                GroupingVariableDTO groupedWith = variableForComponentOpt.get();
                Integer componentIndex = componentIds.get(componentName);
                if (!checkIfAllComponentsInGroupAreValid(groupedWith, componentIndices, componentIndex)) {
                    //log.warn("This partition is not valid. Components in group: {} are not present in groupedWith variable for component: {}, {}", componentIndices, componentName, groupedWith.getDomain());
                    return false;
                }
            }
        }
        //log.info("Result of checking grouped With constraints for group: {}: OK", group.stream().map(PartitionElement::getComponentName));
        return true;
    }


    private boolean checkIfAllComponentsInGroupAreValid(GroupingVariableDTO groupedWithVariableDTO, List<Integer> componentIndices, Integer componentIndex) {
        return groupedWithVariableDTO.getDomain()
                .containsAll(componentIndices.stream().filter(index -> !Objects.equals(index, componentIndex)).collect(Collectors.toList()));

    }


    public boolean checkGroupedWithConstraints(Set<CoalitionElement> group) {
        List<Integer> componentIndices = group.stream()
                .map(element -> componentIds.get(element.getComponentName()))
                .collect(Collectors.toList());
        for (CoalitionElement partitionElement : group) {
            String componentName = partitionElement.getComponentName();
            Optional<GroupingVariableDTO> variableForComponentOpt = data.getGroupedWithVariables().stream()
                    .filter(variable -> componentName.equals(variable.getComponentId()))
                    .findAny();
            if (variableForComponentOpt.isPresent()) {
                GroupingVariableDTO groupedWith = variableForComponentOpt.get();
                Integer componentIndex = componentIds.get(componentName);
                if (!checkIfAllComponentsInGroupAreValid(groupedWith, componentIndices, componentIndex)) {
                    //log.warn("This partition is not valid. Components in group: {} are not present in groupedWith variable for component: {}, {}", componentIndices, componentName, groupedWith.getDomain());
                    return false;
                }
            }
        }
        //log.info("Result of checking grouped With constraints for group: {}: OK", group.stream().map(CoalitionElement::getComponentName));
        return true;
    }

}
