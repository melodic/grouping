package eu.melodic.upperware.utilitygenerator.grouping_generator.service.algorithms;

import eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils;
import eu.melodic.upperware.utilitygenerator.grouping_generator.CoalitionsConverter;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionEvaluatorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GroupingAssignment;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.IntegerPartitionSet;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.AllCoalitionsStructure;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionList;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.CoalitionStructure;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.CoalitionGeneratorService;
import eu.melodic.upperware.utilitygenerator.grouping_generator.service.IntegerPartitionService;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.CombinatoricsUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.ClearedGroupingExperimentsApplication.*;
import static eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.Converters.convertCoalitionStructure;


@Slf4j
public class RahwanAnyTimeAlgorithm implements PartitionService {

    private GrouperData data;
    private List<String> componentsSorted;
    private AllCoalitionsStructure allCoalitions;
    private IntegerPartitionSet allIntegerPartitions;
    private IntegerPartitionSet integerPartitionsThatContainTwoParts;
    private AtomicInteger coalitionStructureCounter;

    public RahwanAnyTimeAlgorithm(GrouperData data, ResourceRequirementsTable resourceRequirementsTable) {
        Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(Level.INFO));

        this.coalitionStructureCounter = new AtomicInteger();
        this.data = data;
        this.componentsSorted = data.getResourceRequirementsTable().getRequirementsList().keySet()
                .stream()
                .sorted()
                .collect(Collectors.toList());

        CoalitionGeneratorService coalitionGeneratorService = new CoalitionGeneratorService();
        this.allCoalitions = coalitionGeneratorService.generateAllCoalitions(componentsSorted, data, false);

        IntegerPartitionService integerPartitionService = new IntegerPartitionService();
        this.allIntegerPartitions = integerPartitionService.generateAllIntegerPartitions(componentsSorted.size());
        this.integerPartitionsThatContainTwoParts = integerPartitionService.filterPartitionsThatContainKParts(allIntegerPartitions, 2);


        //log.info("All feasible coalitions: {}", allCoalitions.getCoalitions().values().stream().mapToInt(coalition -> coalition.getCoalitions().size()).sum());
            //log.info("All coalitions:, size: {}", allCoalitions.getCoalitions());
            //log.debug("All coalitions: {}", allCoalitions.getCoalitions());
         //   log.info("All integer partitions size: {},", allIntegerPartitions.getPartitions().size());
            //log.debug("All integer partitions: {}", allIntegerPartitions);



    }

    //old
    public GroupingAssignment findBestPartition(Collection<VariableValueDTO> solution, PartitionEvaluatorService partitionEvaluatorService) {

        coalitionStructureCounter.set(0);
        //log.info("Using Rahwan Anytime Algorithm...");

        //calculating coalitions values
        /*allCoalitions.getCoalitions().values()
                .forEach(coalitionList -> coalitionList
                        .getCoalitions()
                        .forEach(coalition -> coalition.setCoalitionValue(partitionEvaluatorService.evaluateCoalition(solution, coalition)))
                );
        //todo: powinno być usuwanie tymczasowe tych koalicji, które mają negative infinity wpisane!!
*/
        //calculating properties
        allCoalitions.getCoalitions()
                .values()
                .forEach(list -> list = calculatePropertiesForList(list));



        if (GENERATING_DATA_FOR_ODPIP){
            CoalitionsConverter converter = new CoalitionsConverter(allCoalitions, componentsSorted);
            ArrayList<Coalition> converterCoalitions = converter.getCoalitions();
            double[] coalitionValues = converterCoalitions.stream()
                    .mapToDouble(Coalition::getCoalitionValue)
                    .toArray();


            //System.out.println("Coalition values for solution created:" + solution.stream().map((Function<VariableValueDTO, Number>) VariableValueDTO::getValue).collect(Collectors.toList()).toString());

            return new GroupingAssignment(700.0, Collections.emptyList());

        }


        CoalitionStructure bestCoalitionStructure = scanAndSearch(componentsSorted.size(), allCoalitions);
        //log.info("The best solution found so far is: {}", bestCoalitionStructure);

        //        integer partition, line 31:
        IntegerPartitionSet deltaSet = IntegerPartitionSet.difference(allIntegerPartitions, integerPartitionsThatContainTwoParts);
        deltaSet = calculatePartitionsProperties(allCoalitions, deltaSet);

        double maximumValueForAllPartitions = deltaSet.calculateMaximum();
        double averageValueForAllPartitions = deltaSet.calculateAverage();
        double upperBound = Double.max(maximumValueForAllPartitions, bestCoalitionStructure.getValue());
        double lowerBound = Double.max(averageValueForAllPartitions, bestCoalitionStructure.getValue());
        //log.debug("Upperboud: {}, Lowerbound: {}", upperBound, lowerBound);

        //log.debug("Delta dataset before pruning: {}", deltaSet.getPartitions());

        //line 38 Algorithm 1: prune subspaces: new method from Algorithm 2
        IntegerPartitionSet prunedDataSet = prune(deltaSet, lowerBound);
        //log.debug("The pruned dataset: {}", prunedDataSet.getPartitions());

        //calculate beta: the bound on the quality of the best solution found so far
        double beta = Double.min(componentsSorted.size() / 2, upperBound / bestCoalitionStructure.getValue());

        //log.debug("not - prunedParitions: {}", prunedDataSet.getPartitions().size());

        //Algorithm 3 todo: what should be the beta?
        bestCoalitionStructure = searchSpace(bestCoalitionStructure, upperBound, beta / 100000, prunedDataSet, componentsSorted);
        //GroupingAssignment groupingAssignment = new GroupingAssignment(bestCoalitionStructure.getValue(), convertCoalitionStructure(data.getGroupIdVariables(), bestCoalitionStructure.getStructure()));

        GroupingAssignment groupingAssignment = new GroupingAssignment(bestCoalitionStructure.getValue(), convertCoalitionStructure(data.getGroupIdVariables(), bestCoalitionStructure.getStructure()), coalitionStructureCounter.get());

        //log.info("Grouping Assignment: {}", groupingAssignment.toString());
        log.info("Checked coalition structures: {}", coalitionStructureCounter.get());



        coalitionStructureCounter.set(0);
        return groupingAssignment;
    }



    @Override
    public GroupingAssignment findBestPartition(ResourceRequirementsTable resourceRequirementsTable, PartitionEvaluatorService partitionEvaluatorService) {

        if (GENERATING_DATA_FOR_ODPIP){
            CoalitionGeneratorService coalitionGeneratorService = new CoalitionGeneratorService();
            AllCoalitionsStructure odpipCoalitions = coalitionGeneratorService.generateAllCoalitions(componentsSorted, data, true);
            generateDataForODPIP(odpipCoalitions, resourceRequirementsTable, partitionEvaluatorService );
        }

        coalitionStructureCounter.set(0);
        //log.info("Using Rahwan Anytime Algorithm...");

        //calculating coalitions values
        allCoalitions.getCoalitions().values()
                .forEach(coalitionList -> coalitionList
                        .getCoalitions()
                        .forEach(coalition -> coalition.setCoalitionValue(partitionEvaluatorService.evaluateCoalition(resourceRequirementsTable, coalition)))
                );
        //log.info("Coalitions with values: {}", allCoalitions.getCoalitions().toString());

        allCoalitions.getCoalitions().values()
                .forEach(coalitionList -> coalitionList.getCoalitions().removeIf(coalition -> coalition.getCoalitionValue() == Double.NEGATIVE_INFINITY)
                );
        //log.info("Coalitions with values: {}", allCoalitions.getCoalitions().toString());

        //countinng the number of coalitions available

        avCoalitions = allCoalitions.getCoalitions().values()
                .stream()
                .mapToInt(list -> list.getCoalitions().size())
                .sum();


        //calculating properties
        allCoalitions.getCoalitions()
                .values()
                .forEach(list -> list = calculatePropertiesForList(list));


        CoalitionStructure bestCoalitionStructure = scanAndSearch(componentsSorted.size(), allCoalitions);
        //log.info("The best solution found so far is: {}", bestCoalitionStructure);

        //        integer partition, line 31:
        IntegerPartitionSet deltaSet = IntegerPartitionSet.difference(allIntegerPartitions, integerPartitionsThatContainTwoParts);
        deltaSet = calculatePartitionsProperties(allCoalitions, deltaSet);

        double maximumValueForAllPartitions = deltaSet.calculateMaximum();
        double averageValueForAllPartitions = deltaSet.calculateAverage();
        double upperBound = Double.max(maximumValueForAllPartitions, bestCoalitionStructure.getValue());
        double lowerBound = Double.max(averageValueForAllPartitions, bestCoalitionStructure.getValue());
        //log.debug("Upperboud: {}, Lowerbound: {}", upperBound, lowerBound);

        //log.debug("Delta dataset before pruning: {}", deltaSet.getPartitions());

        //line 38 Algorithm 1: prune subspaces: new method from Algorithm 2
        IntegerPartitionSet prunedDataSet = prune(deltaSet, lowerBound);
        //log.debug("The pruned dataset: {}", prunedDataSet.getPartitions());

        //calculate beta: the bound on the quality of the best solution found so far
        double beta = Double.min(componentsSorted.size() / 2, upperBound / bestCoalitionStructure.getValue());

        //log.debug("not - prunedParitions: {}", prunedDataSet.getPartitions().size());

        //Algorithm 3 todo: what should be the beta?
        bestCoalitionStructure = searchSpace(bestCoalitionStructure, upperBound, beta / 100000, prunedDataSet, componentsSorted);

        GroupingAssignment groupingAssignment = new GroupingAssignment(bestCoalitionStructure.getValue(), convertCoalitionStructure(data.getGroupIdVariables(), bestCoalitionStructure.getStructure()),coalitionStructureCounter.get());

        //log.info("Grouping Assignment: {}", groupingAssignment.toString());
        log.info("Checked coalition structures: {}", coalitionStructureCounter.get());

        coalitionStructureCounter.set(0);
        return groupingAssignment;

    }

    private void generateDataForODPIP(AllCoalitionsStructure coalitions, ResourceRequirementsTable resourceRequirementsTable, PartitionEvaluatorService partitionEvaluatorService){

        //check constraints
        //put negative infinity there
        coalitions.getCoalitions().values()
                .forEach(coalitionList -> coalitionList
                        .getCoalitions()
                        .forEach(coalition -> coalition.setCoalitionValue(partitionEvaluatorService.evaluateCoalition(resourceRequirementsTable, coalition)))
                );

        CoalitionsConverter converter = new CoalitionsConverter(coalitions, componentsSorted);
        ArrayList<Coalition> converterCoalitions = converter.getCoalitions();
        double[] coalitionValues = converterCoalitions.stream()
                .mapToDouble(Coalition::getCoalitionValue)
                .toArray();


        log.info("FOR ODP-IP: Coalitions, list: {}", coalitions.getCoalitions().size());
        //log.info("converted: {}", converterCoalitions.toString());


        try {
            givenDataArray_whenConvertToCSV_thenOutputCreated("/Users/mrozanska/Desktop/odpip-"+componentsSorted.size()+"/"+ODP_IP_FILE_NAME, coalitionValues);
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Coalition values for solution created and stored in the file {}", "/Users/mrozanska/Desktop/odpip"+componentsSorted.size()+"/"+ODP_IP_FILE_NAME);


    }

    private CoalitionList calculatePropertiesForList(CoalitionList coalitions) {

        Stream<Double> coalitionValues = coalitions.getCoalitions()
                .stream()
                .map(Coalition::getCoalitionValue);
        DoubleSummaryStatistics statistics = coalitionValues.mapToDouble(x -> x).summaryStatistics();
        coalitions.setAvgValue(statistics.getAverage());
        coalitions.setMaxValue(statistics.getMax());
        return coalitions;
    }

    private CoalitionStructure scanAndSearch(int numberOfAgents, AllCoalitionsStructure allCoalitions) {

        return IntStream.range(1, numberOfAgents - 1)
                .mapToObj(currentSize -> scanAndSearchOneList(numberOfAgents, allCoalitions, currentSize))
                .max(CoalitionStructure::compareByTheValue)
                .orElseThrow(() -> new IllegalStateException("Something went wrong during scan and search"));

    }

    private CoalitionStructure scanAndSearchOneList(int numberOfAgents, AllCoalitionsStructure allCoalitions, int currentSize) {

        //s^ - line 3
        int secondSize = numberOfAgents - currentSize;
        int numberOfCoalitionsToCheck;

        CoalitionList currentCoalitionList = allCoalitions.getCoalitions().get(currentSize);
        CoalitionList secondCoalitionList = allCoalitions.getCoalitions().get(secondSize);

        if (currentCoalitionList.getCoalitions().isEmpty() || secondCoalitionList.getCoalitions().isEmpty()){
            //log.info("no coalitions for size: {} or {}", currentSize, secondSize);
            return new CoalitionStructure();
        }

        int numberOfCoalitions = currentCoalitionList.getCoalitions().size();
        //log.info("number of coalitions:");
        numberOfCoalitionsToCheck = numberOfCoalitions;

        if (currentSize == secondSize) { //cycling through the same list
            numberOfCoalitionsToCheck = numberOfCoalitions / 2; //it should be lower bound
        }

        double maximumTotalValue = Double.NEGATIVE_INFINITY;
        int indexWithMaximumTotalValue = 0; //xmax

        for (int index = 0; index < numberOfCoalitionsToCheck; index++) { //line 10: cycle through the list v(Ls) i v(Ls'): change - from 0;
            //second index is not clear

            int secondIndex = numberOfCoalitions - index - 1; //change, it was +1

            Coalition coalitionAtIndex = currentCoalitionList.getCoalitions().get(index); //v

            //Coalition coalitionAtSecondIndex = secondCoalitionList.getCoalitions().get(secondIndex); //v^
            Optional<Coalition> optionalCoalitionAtSecondIndex = findMatchingCoalition(secondCoalitionList.getCoalitions(), coalitionAtIndex);

            if (optionalCoalitionAtSecondIndex.isPresent()){
                coalitionStructureCounter.incrementAndGet();
                Coalition coalitionAtSecondIndex = optionalCoalitionAtSecondIndex.get();
                //line 20
                if (maximumTotalValue < coalitionAtIndex.getCoalitionValue() + coalitionAtSecondIndex.getCoalitionValue()) {
                    maximumTotalValue = coalitionAtIndex.getCoalitionValue() + coalitionAtSecondIndex.getCoalitionValue();
                    indexWithMaximumTotalValue = index;
                }
            }

        }

        //line 25:
        int secondIndexWithMaximumTotalValue = numberOfCoalitions - indexWithMaximumTotalValue - 1; //x^max //change: -1

        //line 26:
        if (maximumTotalValue > Double.NEGATIVE_INFINITY){
            //create a CoalitionStructure with coalitions from best indexes; line 26
            //Coalition secondCoalition = secondCoalitionList.getCoalitions().get(secondIndexWithMaximumTotalValue);
            Coalition firstCoalition = currentCoalitionList.getCoalitions().get(indexWithMaximumTotalValue);
            Coalition secondCoalition = findMatchingCoalition(secondCoalitionList.getCoalitions(), firstCoalition).get();


            Collection<Coalition> bestCS = new ArrayList<>();
            bestCS.add(firstCoalition);
            bestCS.add(secondCoalition);
            return new CoalitionStructure(bestCS);
        }
        return new CoalitionStructure();

    }

    private Optional<Coalition> findMatchingCoalition(List<Coalition> list, Coalition coalitionToMatch){
        Collection<String> componentsInFirstCoalitions = coalitionToMatch.getComponentNames();
        List<Coalition> matchedCoalitions = list.stream()
                .filter(coalition -> coalition.getComponentNames()
                            .stream()
                            .noneMatch(componentsInFirstCoalitions::contains))
                .collect(Collectors.toList());
        if (matchedCoalitions.size() > 1 ){
            log.error("there are more than one matched coalitions for coalition: {}, {}", coalitionToMatch, matchedCoalitions);
            throw new IllegalStateException(String.format("there are more than one matched coalitions for coalition: {}, {}", coalitionToMatch, matchedCoalitions));
        }
        else if (matchedCoalitions.size() == 0){
            //log.info("there is no matching coalition for: {}", coalitionToMatch);
            return Optional.empty();
        }
        else {
            return Optional.of(matchedCoalitions.get(0));
        }

    }



    private IntegerPartitionSet calculatePartitionsProperties(AllCoalitionsStructure allCoalitions, IntegerPartitionSet partitions) {
        for (IntegerPartition partition : partitions.getPartitions()) {
            //lines 36 and 37
            double maximumForPartition = 0;
            double avgForPartition = 0;
            for (int part : partition.getPattern()) {
                double maxValueForPart = allCoalitions.getCoalitions().get(part).getMaxValue();
                double avgValueForPart = allCoalitions.getCoalitions().get(part).getAvgValue();
                maximumForPartition += maxValueForPart;
                avgForPartition += avgValueForPart;
            }
            partition.setAverage(avgForPartition);
            partition.setMaximum(maximumForPartition);
        }
        return partitions;
    }

    //prune the subspaces that have an upper bound lower than LB*
    private IntegerPartitionSet prune(IntegerPartitionSet integerPartitionSet, double lowerBound) {
        IntegerPartitionSet pruned = new IntegerPartitionSet(integerPartitionSet.getPartitions(), integerPartitionSet.getNumber());
        pruned.getPartitions().removeIf(integerPartition -> integerPartition.getMaximum() <= lowerBound);
        return pruned;
    }


    //search, or prune, the remaining sub-spaces
    //UB*, beta, G, A,
    private CoalitionStructure searchSpace(CoalitionStructure currentBestCoalitionStructure, double upperBound, double beta, IntegerPartitionSet integerPartitionSet,
            List<String> agents) {

        //log.debug("Searching space for integer partition set: {}", integerPartitionSet.getPartitions());
        while (!integerPartitionSet.getPartitions().isEmpty()) {

            CoalitionStructure bestCoalitionStructure;

            IntegerPartition integerPartition = integerPartitionSet.getPartitions().get(0);

            //log.debug("Considering integer partition: {}", integerPartition);
            bestCoalitionStructure = searchList(upperBound, beta, integerPartition,
                        0, 0, agents, currentBestCoalitionStructure, new ArrayList<>());
                //line 4:
                integerPartitionSet.getPartitions().remove(integerPartition);
//            }
            if (currentBestCoalitionStructure.getValue() != bestCoalitionStructure.getValue()) {
                //log.info("There is a new best coalition structure: {}", bestCoalitionStructure);
                currentBestCoalitionStructure = bestCoalitionStructure;
                prune(integerPartitionSet, bestCoalitionStructure.getValue());
            }
            //line 8:
            double maxForCurrentIntegerPartitionSet = integerPartitionSet
                    .getPartitions()
                    .stream()
                    .mapToDouble(IntegerPartition::getMaximum)
                    .max()
                    .orElse(Double.NEGATIVE_INFINITY);
            upperBound = Double.max(bestCoalitionStructure.getValue(), maxForCurrentIntegerPartitionSet);
            double newBeta = Double.min(beta, upperBound / bestCoalitionStructure.getValue());

            if (newBeta < beta) { //if bestCoalitionStructure is within the specified bound from optimal
                return bestCoalitionStructure;
            }
        }
        return currentBestCoalitionStructure;
    }

    //search a subspace: UB*, beta*, MAXg, G, k, alfa, Ak, CS', CS->
    private CoalitionStructure searchList(double upperBound, double beta, IntegerPartition partition,
            int index, int alfa, List<String> agents, CoalitionStructure bestCoalitionStructure,
            List<Coalition> unfinishedCoalitionStructure) {

        //log.debug("New searchingList: index: {}, alfa: {}, unfinishedCoalition: {}",
                //index, alfa, unfinishedCoalitionStructure);
        int currentPart = partition.getPattern().get(index);
        //log.debug("Searching list for current part of partition: {}, index: {}", currentPart, index);
        //log.debug("Agents that are not used: {}", agents);
        if (index > 0 && currentPart != partition.getPattern().get(index - 1)) {
             alfa = 0;
        }

        Iterator<int[]> combinationsIterator = CombinatoricsUtils.combinationsIterator(agents.size(), currentPart);
        int counter = 0;
        int alfaForDeeperSearch=alfa;

        while (combinationsIterator.hasNext()) {
            int[] combination = combinationsIterator.next();
            //log.debug("Checking combination: {}", combination);

            long elementsOfTheSameSize = partition.getPattern().stream().filter(element -> element == currentPart).count();
            long sumElementsOfTheSameSize = countRepeatedPartitionPatterns(partition, index);

            long numberOfAgents = partition.getPattern().stream().mapToInt(element -> element).sum();

            if (combination[0] > numberOfAgents - sumElementsOfTheSameSize){ //line4://n+1 - sum (gk x G(gk))
                // old version: if (combination[0] > numberOfAgents+1 - sumElementsOfTheSameSize){ //n+1 - sum (gk x G(gk))
                continue;
            }
            else if (combination[0] < alfa){ //a<= Mk,1
                //log.debug("alfa skipping combination {}", combination);
                continue;
            }

            if (counter == 0) {//this is the smallest coalition, Mk,1 or new alfa
                alfaForDeeperSearch = combination[0];
                //log.debug("Alfa for future search: {}", alfaForDeeperSearch);
            }
            counter++;

            Collection<String> agentsInCoalition = findAgentsInCoalition(agents, combination);
            Optional<Coalition> optionalCoalition = toCoalition(this.allCoalitions, agentsInCoalition);
            if (!optionalCoalition.isPresent()){
               // log.info("no coalition for this combination, continuing searching...");
                continue;
            }
            Coalition coalition = optionalCoalition.get();
            //log.debug("Current coalition: {}", coalition.getElements());

            List<Coalition> newUnfinishedCoalitionStructure = new ArrayList<>(unfinishedCoalitionStructure);
            newUnfinishedCoalitionStructure.add(coalition);
            double currentCoalitionStructureValue = countCoalitionStructureValue(newUnfinishedCoalitionStructure);
            //log.debug("Current coalition structure value: {} : {}", currentCoalitionStructureValue, newUnfinishedCoalitionStructure);
            //line 6: //if this is the last coalition to compose the coalition structure
            if (index == partition.getPattern().size() - 1) {
                //log.debug("It is the last coalition to compose the coalition structure");

                coalitionStructureCounter.incrementAndGet();
                if (bestCoalitionStructure.getValue() < currentCoalitionStructureValue) {
                    bestCoalitionStructure = new CoalitionStructure(newUnfinishedCoalitionStructure);
                    //log.debug("Found better coalition structure value: {}, updating: {}", currentCoalitionStructureValue, bestCoalitionStructure);
                }
            }
            //line 8: if there is potential of finding a coalition structure better than current best
            else if (bestCoalitionStructure.getValue() < currentCoalitionStructureValue + countMaximumValueOfUnseenCoalitions(allCoalitions, partition, index)) {

                //log.debug("Searching further...");

                List<String> reducedAgents = new ArrayList<>(agents);
                reducedAgents.removeAll(agentsInCoalition);
                bestCoalitionStructure = searchList(upperBound, beta, partition, index + 1,
                        alfaForDeeperSearch, reducedAgents, bestCoalitionStructure, newUnfinishedCoalitionStructure);

            }
            //line 11: stop if the required solution has been found
            // or if the current best is equal to the upperbound of this subspace
            if ((upperBound / bestCoalitionStructure.getValue() <= beta) || bestCoalitionStructure.getValue() == partition.getMaximum()) {
                //log.info("The best coalition structure for given bound beta = {}, has been found:{}",
                 //       beta, bestCoalitionStructure);
                return bestCoalitionStructure;
            }
        }
        //log.debug("Nothing interesting found, returning:{}", bestCoalitionStructure);
        return bestCoalitionStructure;

    }

    private Collection<String> findAgentsInCoalition(List<String> agents, int[] combination) {
        agents = agents.stream().sorted().collect(Collectors.toList());
        Collection<String> agentsInCoalition = new ArrayList<>();
        for (int aCombination : combination) {
            String agent = agents.get(aCombination);
            agentsInCoalition.add(agent);
        }
        return agentsInCoalition;
    }

    private Optional<Coalition> toCoalition(AllCoalitionsStructure allCoalitionsStructure, Collection<String> agentsInCoalition) {
        List<Coalition> coalitions = allCoalitionsStructure.getCoalitions().get(agentsInCoalition.size()).getCoalitions();


        Optional<Coalition> optionalCoalition = coalitions.stream()
                .filter(coalition -> coalition.getElements()
                        .stream()
                        .allMatch(element -> agentsInCoalition.contains(element.getComponentName())))
                .findAny();

        if (!optionalCoalition.isPresent()) {

            //return optionalCoalition.get();
            //log.info("no present coalition for agents: {}", agentsInCoalition);
            //throw new IllegalStateException("Coalition for agents: " + agentsInCoalition + " is not found in the set of all coalitions");
        }
        return optionalCoalition;
    }

    private double countCoalitionStructureValue(Collection<Coalition> coalitionStructure) {
        if (coalitionStructure.isEmpty()) {
            return Double.NEGATIVE_INFINITY;

        }
        return coalitionStructure.stream().mapToDouble(Coalition::getCoalitionValue).sum();
    }

    private double countMaximumValueOfUnseenCoalitions(AllCoalitionsStructure allCoalitionsStructure, IntegerPartition partition, int index) {
        List<Integer> notCheckedElementsOfIntegerPartition = partition.getPattern().subList(index + 1, partition.getPattern().size());
        //log.debug("List of notChecked Elements of Integer partition: {}", notCheckedElementsOfIntegerPartition);
        return notCheckedElementsOfIntegerPartition.stream()
                .mapToDouble(element -> allCoalitionsStructure.getCoalitions().get(element).getMaxValue())
                .sum();

    }

    private void givenDataArray_whenConvertToCSV_thenOutputCreated(String fileName, double[] values) throws IOException {

        //log.info("filename:{}", fileName);
        File csvOutputFile = new File(fileName);

        List<String[]> dataLines = new ArrayList<>();

        String[] array = new String[1];
        array[0] = "0";
        dataLines.add(array);
        for (double value: values){
            String[] array1 = new String[1];
            array1[0] = Double.toString(value);
            dataLines.add(array1);
        }
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
        //assertTrue(csvOutputFile.exists());
    }

    private String convertToCSV(String[] data) {
        return Stream.of(data)
                .collect(Collectors.joining(","));
    }


    private int countRepeatedPartitionPatterns(IntegerPartition partition, int maxIndex){
        int result = 0;
       // List<Integer>  mozliwe, ze za dużo razy będzie odejmowane, np. g1 i g2 są takie same, to odejmiemy dwa razy te liczniki
        List<Integer> countedElements = new ArrayList<>();
        for (int index =0; index<=maxIndex; index++){
            Integer currentPart = partition.getPattern()
                    .get(index);
            if (countedElements.contains(currentPart)){
                //log.info("This element has been counted, continuing...");

            }
            else {
                long elementsOfTheSameSize = partition.getPattern()
                        .stream()
                        .filter(element -> element.equals(currentPart))
                        .count();
                result += currentPart * elementsOfTheSameSize;
                countedElements.add(currentPart);
            }

        }
        //log.info("sum gk * G(gk): for partition{}, maxIndex {} is {}", partition.getPattern().toString(), maxIndex, result);
        return result;

    }

}
