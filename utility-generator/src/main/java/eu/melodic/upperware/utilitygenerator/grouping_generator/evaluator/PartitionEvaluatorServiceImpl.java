package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.UtilityGeneratorApplication;
import eu.melodic.upperware.utilitygenerator.evaluator.ConfigurationElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.data_for_experiments.ResourceRequirementsTable;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.GrouperData;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.coalition.Coalition;
import eu.melodic.upperware.utilitygenerator.node_candidates.NodeCandidatesSelector;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator.PartitionToConfigurationElementConverter.findComponentsInGroup;

@Slf4j
@AllArgsConstructor
public class PartitionEvaluatorServiceImpl implements PartitionEvaluatorService {

    //private Collection<VariableDTO> variables;
    //private NodeCandidatesSelector nodeCandidatesSelector;
    private UtilityGeneratorApplication utilityGenerator;
    private ConstraintChecker constraintChecker;


    public PartitionEvaluatorServiceImpl(UtilityGeneratorApplication utilityGenerator, GrouperData data) {
        //this.variables = data.getAllVariables();
        this.utilityGenerator = utilityGenerator;
        //this.nodeCandidatesSelector = new NodeCandidatesSelector(data.getNodeCandidates());
        this.constraintChecker = new ConstraintChecker(data);
    }

    //@Override
    /*public double evaluatePartition(Collection<VariableValueDTO> solution, Collection<PartitionElement> partition) {

        //log.debug("Evaluating partition: {}", partition.toString());
        boolean validPartition = constraintChecker.checkConstraintsSatisfaction(solution, partition);
        //log.debug("Result of validating parition: {}", validPartition);

        if (validPartition) {

            Collection<ConfigurationElement> configurationToEvaluate = new ArrayList<>();

            //find all partitions:
            int numberOfComponents = partition.size();

            PartitionToConfigurationElementConverter converter = new PartitionToConfigurationElementConverter(variables);

            //for each group
            for (int i = 0; i < numberOfComponents; i++) {


                Collection<PartitionElement> group = findComponentsInGroup(i, partition);
                int groupSize = group.size();
                //log.debug("The number of components in one group: {}", groupSize);

                if (groupSize > 0) {
                    NodeCandidate nodeCandidateForGroup = nodeCandidatesSelector.getNodeCandidateForGroup(group, solution, variables);
                    configurationToEvaluate.addAll(converter.convertToConfigurationElementList(solution, group, nodeCandidateForGroup));
                }
            }

            return utilityGenerator.evaluatePartition(solution, configurationToEvaluate);
        } else {
            log.warn("This is not a valid partition, returning 0.0 as utility value");
            return 0.0;
        }
    }



    @Override
    public double evaluateCoalition(Collection<VariableValueDTO> solution, Coalition coalition) {

        //log.debug("Evaluating coalition: {}", coalition.toString());

        //if not generating, then these coalitions are not present
        if (GENERATING_DATA_FOR_ODPIP){
            if (!constraintChecker.checkGroupedWithConstraints(coalition.getElements())){
                //log.info("not valid coalition, returning -infinity");
                return Double.NEGATIVE_INFINITY;
            }
        }

        List<PartitionElement> coalitionAsPartitionElements = coalition.getElements()
                .stream()
                .map(coalitionElement -> new PartitionElement("", coalitionElement.getComponentName(), 0, null))
                .collect(Collectors.toList());

        NodeCandidate nodeCandidate = nodeCandidatesSelector.getNodeCandidateForGroup(coalitionAsPartitionElements, solution, variables);
        if (nodeCandidate == null){
            //log.warn("No Node Candidate for this group");
            return Double.NEGATIVE_INFINITY;
        }
        else{
            //log.debug("ID of Found NC: {}, price: {}, ram: {}", nodeCandidate.getId(), nodeCandidate.getPrice(), nodeCandidate.getHardware().getRam());
            double costUtility = -1.0/nodeCandidate.getPrice();

            //double normalizedCostUtility = (nodeCandidate.getPrice() / (nodeCandidatesSelector.getMaxPrice() - nodeCandidatesSelector.getMinPrice()));
            double normalizedCostUtility =  - nodeCandidate.getPrice() / (nodeCandidatesSelector.getMaxPrice() - nodeCandidatesSelector.getMinPrice());

            //log.info("cost Utility = {}", costUtility);
            //log.info("normalized Cost utility = {}", normalizedCostUtility);
            return normalizedCostUtility;
        }
    }*/

    @Override
    public double evaluateCoalition(ResourceRequirementsTable requirementsTable, Coalition coalition) {
        return 0;
    }
    @Override
    public double evaluatePartition(ResourceRequirementsTable requirementsTable, Collection<PartitionElement> partition) {
        return 0;
    }
}
