package eu.melodic.upperware.utilitygenerator.grouping_generator.evaluator;

import eu.melodic.upperware.utilitygenerator.evaluator.ConfigurationElement;
import eu.melodic.upperware.utilitygenerator.grouping_generator.model.PartitionElement;
import eu.paasage.upperware.metamodel.cp.VariableType;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableDTO;
import eu.passage.upperware.commons.model.tools.cpmodel.DTO.VariableValueDTO;
import io.github.cloudiator.rest.model.NodeCandidate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.melodic.upperware.utilitygenerator.evaluator.EvaluatingUtils.getMustHaveVariableValueForComponent;


@AllArgsConstructor
@Slf4j
public class PartitionToConfigurationElementConverter {

    private Collection<VariableDTO> variables;


    public Collection<ConfigurationElement> convertToConfigurationElementList(Collection<VariableValueDTO> solution, Collection<PartitionElement> group, NodeCandidate nodeCandidate) {
        return group.stream().map(partitionElement ->
                new ConfigurationElement(
                        partitionElement.getComponentName(),
                        nodeCandidate,
                        getMustHaveVariableValueForComponent(variables, solution, partitionElement.getComponentName(), VariableType.CARDINALITY)))
                .collect(Collectors.toList());

    }

    static Set<PartitionElement> findComponentsInGroup(int groupId, Collection<PartitionElement> partition) {
        return partition.stream()
                .filter(element -> groupId == element.getGroupNumber())
                .collect(Collectors.toSet());
    }

    static Collection<Set<PartitionElement>> groupComponents(Collection<PartitionElement> partition) {

        int numberOfComponents = partition.size();
        Collection<Set<PartitionElement>> groupedComponents = new ArrayList<>();

        for (int i = 0; i < numberOfComponents; i++) {


            Set<PartitionElement> group = findComponentsInGroup(i, partition);
            int groupSize = group.size();
            //log.info("The number of components in one group: {}", groupSize);

            if (groupSize > 0) {
                groupedComponents.add(group);
            }
        }
        return groupedComponents;
    }

}
